package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func maxDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}

	left := maxDepth(root.Left)
	right := maxDepth(root.Right)

	if left > right {
		return left + 1
	}
	return right + 1
}


func main() {
	root := &TreeNode{Val: 3}
	root.Left = &TreeNode{Val: 9}

	right := &TreeNode{Val: 20}
	right.Left = &TreeNode{Val: 15}
	right.Right = &TreeNode{Val: 7}
	root.Right = right

	fmt.Println(maxDepth(root))
}