package main

import (
	"fmt"
	"sort"
)

func findMinArrowShots(points [][]int) int {
	if len(points) == 0 {
		return 0
	}

	sort.Slice(points, func(i, j int) bool {
		return points[i][1] < points[j][1]
	})

	n := len(points)
	var (
		total = 1
		prev = points[0][1]
	)
	for  i := 1; i < n; i++ {
		if points[i][0] > prev {
			total += 1
			prev =  points[i][1]
		}
	}


	return total
}


func main() {
	points := [][]int{{1, 2}, {2, 3}, {3, 4}, {4, 5}}
	fmt.Println(findMinArrowShots(points))
}
