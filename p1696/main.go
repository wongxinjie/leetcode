package main

import "fmt"

type Queue struct {
	items []int
}

func (q *Queue) AddLast(v int) {
	q.items = append(q.items, v)
}

func (q *Queue) First() int {
	return q.items[0]
}

func (q *Queue) Last() int {
	i := len(q.items)
	return q.items[i-1]
}

func (q *Queue) RemoveFirst() int {
	v := q.items[0]
	q.items = q.items[1:]
	return v
}

func (q *Queue) RemoveLast() int {
	i := len(q.items) - 1
	v := q.items[i]
	q.items = q.items[:i]
	return v
}

func (q *Queue) IsEmpty() bool {
	return len(q.items) == 0
}

func maxResult(nums []int, k int) int {
	if len(nums) == 0 || k <= 0 {
		return 0
	}

	q := &Queue{}
	size := len(nums)
	windows := make([]int, size)
	for i := 0; i < size; i++ {
		if i-k > 0 {
			if !q.IsEmpty() && q.First() == windows[i-k-1] {
				q.RemoveFirst()
			}
		}
		var prev int
		if !q.IsEmpty() {
			prev = q.First()
		}
		windows[i] = prev + nums[i]

		for !q.IsEmpty() && q.Last() < windows[i] {
			q.RemoveLast()
		}
		q.AddLast(windows[i])
	}

	return windows[size-1]
}

func main() {
	nums := []int{10, -5, -2, 4, 0, 3}
	k := 3
	fmt.Println(maxResult(nums, k))
}
