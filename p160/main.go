package main

import "fmt"

type ListNode struct {
	Val int
	Next *ListNode
}

func getIntersectionNode(headA, headB *ListNode) *ListNode {
	pa, pb := headA, headB
	for pa != pb {
		if pa != nil {
			pa = pa.Next
		} else {
			pa = headB
		}
		if pb != nil {
			pb = pb.Next
		} else {
			pb = headA
		}
	}
	return pa
}

func main() {
	x := &ListNode{Val: 4}
	x.Next = &ListNode{Val: 1}

	y := &ListNode{Val: 5}
	y.Next = &ListNode{Val: 0}
	y.Next.Next = &ListNode{Val: 1}

	c := &ListNode{Val: 8}
	c.Next = &ListNode{Val: 4}
	c.Next.Next = &ListNode{Val: 5}

	//x.Next.Next = c
	//y.Next.Next.Next = c

	p := getIntersectionNode(x, y )
	if p != nil {
		fmt.Println(p.Val)
	} else {
		fmt.Println(p)
	}
}
