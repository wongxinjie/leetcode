package main

import "fmt"

func canPartition(nums []int) bool {
	var total int
	for _, n := range nums {
		total += n
	}
	if total%2 != 0 {
		return false
	}

	target := total / 2
	n := len(nums)
	dp := make([]bool, target+1)
	dp[0] = true

	for i := 1; i <= n; i++ {
		for j := target; j >= nums[i-1]; j-- {
			dp[j] = dp[j] || dp[j-nums[i-1]]

		}
	}
	return dp[target]
}

func main() {
	fmt.Println(canPartition([]int{1, 5, 11, 5}))
}
