package main

import "fmt"

func isBipartite(graph [][]int) bool {
	size := len(graph)
	if size == 0 {
		return true
	}

	// 0 表示未检查节点
	color := make([]int, size)
	queue := make([]int, 0)
	for i := 0; i < size; i++ {
		if color[i] == 0 {
			queue = append(queue, i)
			color[i] = 1
		}

		for len(queue) > 0 {
			v := queue[0]
			queue = queue[1:]

			edges := graph[v]
			for _, edg := range edges {
				// 进行染色
				if color[edg] == 0 {
					queue = append(queue, edg)
					if color[v] == 1 {
						color[edg] = 2
					} else {
						color[edg] = 1
					}
				} else if color[v] == color[edg] {
					return false
				}
			}
		}
	}
	return true
}

func main()  {
	graph := [][]int{{1, 2, 3}, {0, 2}, {0, 1, 3}, {0, 2}}
	fmt.Println(isBipartite(graph))
	
}
