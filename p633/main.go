package main

import (
	"fmt"
	"math"
)

func judgeSquareSum(c int) bool {
	if c == 1 {
		return true
	}

	b, e := 0, int(math.Sqrt(float64(c))) + 1
	for b <= e {
		rv := b * b + e * e
		if rv == c {
			return true
		} else if rv < c {
			b += 1
		} else {
			e -= 1
		}
	}
	return false
}

func main() {
	fmt.Println(judgeSquareSum(1))
}
