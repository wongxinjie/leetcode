package main

import "fmt"

func getLeastNumbers(arr []int, k int) []int {

	target := kth(arr, 0, len(arr), k)

	ans := make([]int, k)
	var equalKth, idx int
	for _, n := range arr {
		if n < target {
			ans[idx] = n
			idx++
		}
		if n == target {
			equalKth++
		}
	}

	for idx < k && equalKth > 0 {
		ans[idx] = target
		idx++
		equalKth--
	}
	return ans
}

func kth(arr []int, begin, end, k int) int {
	if begin >= end {
		return 0
	}
	if begin+1 >= end {
		return arr[begin]
	}

	mid := begin + (end-begin)/2
	x := arr[mid]

	var (
		left  = begin
		i     = begin
		right = end - 1
	)
	for i <= right {
		if arr[i] < x {
			arr[left], arr[i] = arr[i], arr[left]
			i++
			left++
		} else if arr[i] == x {
			i++
		} else {
			arr[right], arr[i] = arr[i], arr[right]
			right--
		}
	}

	if left-begin >= k {
		return kth(arr, begin, left, k)
	}
	if i-begin >= k {
		return arr[left]
	}

	return kth(arr, i, end, k-(i-begin))
}

func main() {
	nums := []int{0, 1, 1, 2, 4, 4, 1, 3, 3, 2}
	k := 6
	fmt.Println(getLeastNumbers(nums, k))
}
