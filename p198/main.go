package main

import "fmt"

func rob(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	n := len(nums)
	if n == 1 {
		return nums[0]
	}
	var (
		x, y, current int
	)
	for i := 0; i < n; i++ {
		if x + nums[i] > y {
			current = x + nums[i]
		} else {
			current = y
		}
		x = y
		y = current
	}
	return current
}


func main() {
	in := []int{2, 7, 9, 3, 1}
	fmt.Println(rob(in))
}