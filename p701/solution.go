package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}


func insertIntoBST(root *TreeNode, val int) *TreeNode {
	if root == nil {
		return &TreeNode{Val: val}
	}

	current := root
	for current != nil {
		if current.Val > val {
			if current.Left != nil {
				current = current.Left
			} else {
				current.Left = &TreeNode{Val: val}
				break
			}
		} else {
			if current.Right != nil {
				current = current.Right
			} else {
				current.Right = &TreeNode{Val: val}
				break
			}
		}
	}
	return root
}

func inOrder(root *TreeNode) {
	if root == nil {
		return
	}
	inOrder(root.Left)
	fmt.Println(root.Val)
	inOrder(root.Right)
}

func main() {
	nums := []int{8, 3, 2, 1, 7, 5, 9}
	var root *TreeNode
	for _, n := range nums {
		root = insertIntoBST(root, n)
	}
	inOrder(root)

}