package main

import "fmt"

var keyBoard = map[string]string{
	"2": "abc",
	"3": "def",
	"4": "ghi",
	"5": "jkl",
	"6": "mno",
	"7": "pqrs",
	"8": "tuv",
	"9": "wxyz",
}

// https://leetcode-cn.com/problems/letter-combinations-of-a-phone-number/description/
func letterCombinations(digits string) []string {
	if len(digits) == 0 {
		return make([]string, 0)
	}

	box := make([]rune, 0)
	letters := make([]string, 0)
	backTrack(digits, 0, &box, &letters)
	return letters
}

// 非常典型的回溯算法模版
func backTrack(digits string, idx int, box *[]rune, letters *[]string) {
	size := len(digits)
	if size == 0 {
		return
	}

	if len(*box) == size {
		*letters = append(*letters, string(*box))
	}
	if idx >= size {
		return
	}

	chars := []rune(keyBoard[string(digits[idx])])
	for i := 0; i < len(chars); i++ {
		c := chars[i]
		*box = append(*box, c)
		backTrack(digits, idx+1, box, letters)
		*box = (*box)[:len(*box)-1]
	}
}

func main() {
	digits := "2"
	fmt.Println(letterCombinations(digits))
}
