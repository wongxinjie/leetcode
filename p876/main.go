package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func middleNode(head *ListNode) *ListNode {
	if head.Next == nil {
		return head
	}

	fast, slow := head, head
	for fast != nil && fast.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
	}
	return slow
}

func main() {
	head := &ListNode{Val: 1}
	nums := []int{2, 3, 4, 5, 6}

	current := head
	for _, n := range nums {
		node := &ListNode{Val: n}
		current.Next = node
		current = node
	}

	node := middleNode(head)
	fmt.Println(node.Val)
}
