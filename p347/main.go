package main

import "fmt"

func topKFrequent(nums []int, k int) []int {
	counter := make(map[int]int)
	maxCount := 0

	for _, n := range nums {
		counter[n] += 1
		if counter[n] > maxCount {
			maxCount = counter[n]
		}
	}

	buckets := make([][]int, maxCount+1)
	for k, v := range counter{
		buckets[v] = append(buckets[v], k)
	}

	result := make([]int, 0)
	for i := maxCount; i >=0&& len(result) < k; i--{
		for _, n := range buckets[i] {
			result = append(result, n)
			if len(result) == k {
				break
			}
		}
	}
	return result
}

func main() {
	nums := []int{1, 1, 1, 1, 2, 2, 3, 4}
	fmt.Println(topKFrequent(nums, 2))
}