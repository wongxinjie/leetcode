package main

import "fmt"

const (
	charCount = 26
)
type TrieNode struct {
	val      rune
	children []*TrieNode
	isLeaf   bool
}

func NewTrieNode() *TrieNode {
	return &TrieNode{
		val:      0,
		children:  make([]*TrieNode, charCount),
		isLeaf:   false,
	}
}

type Trie struct {
	root *TrieNode
}


func Constructor() Trie {
	return Trie{root: NewTrieNode()}
}

func (tr *Trie) Insert(word string) {
	runes := []rune(word)

	current := tr.root
	for _, c := range runes {
		idx := c - 'a'
		node := current.children[idx]
		if node == nil {
			node = &TrieNode{
				val:      c,
				children: make([]*TrieNode, charCount),
				isLeaf:   false,
			}
			current.children[idx] = node
		}
		current = node
	}
	current.isLeaf = true
}

func (tr *Trie) Search(word string) bool {
	current := tr.root

	runes := []rune(word)
	for _, c := range runes {
		idx := c - 'a'
		node := current.children[idx]
		if node == nil {
			return false
		}
		current = node
	}
	return current.isLeaf
}

func (tr *Trie) StartsWith(prefix string) bool {
	runes := []rune(prefix)

	current := tr.root
	for _, c := range runes{
		node := current.children[c - 'a']
		if node == nil {
			return false
		}
		current = node
	}
	return true
}

func main() {
	tr := Constructor()
	tr.Insert("apple")
	fmt.Println(tr.Search("apple"))
	fmt.Println(tr.Search("app"))
	fmt.Println(tr.StartsWith("app"))
	tr.Insert("app")
	fmt.Println(tr.Search("app"))
	fmt.Println(tr.StartsWith("app"))
}

/**
 * Your Trie object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Insert(word);
 * param_2 := obj.Search(word);
 * param_3 := obj.StartsWith(prefix);
 */
