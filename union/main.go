package main

import "fmt"

type GangUnion struct {
	people []int
	count  int
}

func New(n int) *GangUnion {
	people := make([]int, 0, n)
	for i := 0; i < n; i++ {
		people = append(people, i)
	}
	return &GangUnion{
		people: people,
		count:  n,
	}
}

func (u *GangUnion) Find(x int) int {
	if x == u.people[x] {
		return x
	}

	return u.Find(u.people[x])
}

func (u *GangUnion) Union(x, y int) {
	if u.Find(x) != u.Find(y) {
		u.count--
	}
	u.people[y] = u.Find(x)
}

func (u *GangUnion) Count() int {
	return u.count
}

func findGangCount(n int, conn [][]int) int {
	union := New(n)

	for _, c := range conn {
		union.Union(c[0], c[1])
	}
	return union.count
}

func main() {
	conn := [][]int{{1, 2}, {2, 3}}
	fmt.Println(findGangCount(4, conn))
}
