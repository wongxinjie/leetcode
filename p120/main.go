package main

import "fmt"

func minimumTotal(triangle [][]int) int {
	if len(triangle) == 0 || len(triangle[0]) == 0 {
		return 0
	}

	size := len(triangle)
	dp := make([][]int, size)
	for i := 0; i < size; i++ {
		for j := 0; j < len(triangle[i]); j++ {
			if dp[i] == nil {
				dp[i] = make([]int, len(triangle[i]))
			}
			dp[i][j] = triangle[i][j]
		}
	}

	// 自底向上更简洁一点
	for i := len(triangle) - 2; i >= 0; i-- {
		for j := 0; j < len(triangle[i]); j++ {
			dp[i][j] = triangle[i][j]
			if dp[i+1][j] < dp[i+1][j+1] {
				dp[i][j] += dp[i+1][j]
			} else {
				dp[i][j] += dp[i+1][j+1]
			}
		}
	}
	return dp[0][0]
}

func main() {
	triangle := [][]int{{-1}, {2, 3}, {1, -1, -3}}
	fmt.Println(minimumTotal(triangle))
}
