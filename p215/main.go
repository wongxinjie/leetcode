package main

import "fmt"

func findKthLargest(nums []int, k int) int {
	var (
		left = 0
		right = len(nums) -1
		target = len(nums) - k
	)
	for left < right {
		mid := selection(nums, left, right)
		if mid == target {
			return nums[mid]
		}
		if mid < target {
			left = mid + 1
		} else {
			right = mid - 1
		}
	}
	return nums[left]
}

func selection(nums []int, left, right int) int {
	i, j := left + 1, right
	for {
		// 向右查找第一个大于 left 的数
		for i < right && nums[i] <= nums[left] {
			i++
		}
		// 向左查找第一个小于 left 的数
		for j > left && nums[j] >= nums[left] {
			j--
		}
		if i >= j {
			break
		}
		nums[i], nums[j] = nums[j], nums[i]
	}
	nums[left], nums[j] = nums[j], nums[left]
	return j
}

func main() {
	nums := []int{3, 2, 1, 5, 6, 4}
	fmt.Println(findKthLargest(nums, 2))
}