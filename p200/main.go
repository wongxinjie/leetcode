package main

import "fmt"

type Union struct {
	values []int
	count  int
}

func New(n int) *Union {
	values := make([]int, n)
	for i := 0; i < n; i++ {
		values[i] = i
	}
	return &Union{
		values: values,
		count:  n,
	}
}

func (u *Union) Find(x int) int {
	if x == u.values[x] {
		return x
	}

	return u.Find(u.values[x])
}

func (u *Union) Union(x, y int) {
	if u.Find(x) != u.Find(y) {
		u.count--
	}
	u.values[u.Find(y)] = u.Find(x)
}

func numIslands(grid [][]byte) int {
	if len(grid) == 0 || len(grid[0]) == 0 {
		return 0
	}

	var (
		rowCount   = len(grid)
		colCount   = len(grid[0])
		conn       = New(rowCount * colCount)
		waterBlock int
		directions = [][]int{{0, 1}, {1, 0}}
	)

	for i := 0; i < rowCount; i++ {
		for j := 0; j < colCount; j++ {
			if grid[i][j] == '1' {
				for d := 0; d < 2; d++ {
					x := i + directions[d][0]
					y := j + directions[d][1]

					if x >= 0 && x < rowCount && y >= 0 && y < colCount {
						if grid[x][y] == '1' {
							conn.Union(i*colCount+j, x*colCount+y)
						}
					}
				}
			} else {
				waterBlock += 1
			}
		}
	}
	return conn.count - waterBlock
}

func main() {
	grid := [][]byte{
		{'1', '1', '0', '0', '0'},
		{'1', '1', '0', '0', '0'},
		{'0', '0', '1', '0', '0'},
		{'0', '0', '0', '1', '1'},
	}
	fmt.Println(numIslands(grid))
}
