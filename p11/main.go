package main

import "fmt"

func maxArea(height []int) int {

	var area int
	s, e := 0, len(height)-1
	for s < e {
		currentHeight := height[s]
		if currentHeight > height[e] {
			currentHeight = height[e]
		}

		currentArea := (e - s) * currentHeight
		if currentArea > area {
			area = currentArea
		}
		if height[s] > height[e] {
			e--
		} else {
			s++
		}

	}
	return area
}

func main() {
	height := []int{4, 3, 2, 1, 4}
	fmt.Println(maxArea(height))
}
