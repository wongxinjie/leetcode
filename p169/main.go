package main

import (
	"fmt"
	"math"
)

func majorityElement(nums []int) int {

	count := 0
	target := math.MaxInt32

	for _, n := range nums {
		if count == 0 {
			target = n
		}

		if n == target {
			count += 1
		} else {
			count -= 1
		}
	}
	return target
}

func main() {
	nums := []int{3, 2, 3}
	fmt.Println(majorityElement(nums))
}
