package main

import "fmt"

// https://leetcode-cn.com/problems/longest-valid-parentheses/

func longestValidParentheses(s string) int {
	if len(s) == 0 {
		return 0
	}

	stack := make([]int, 0)

	var count, start int
	for i, c := range s {
		if c == '(' {
			// 如果是左括号，直接如队列
			stack = append(stack, i)
		} else {
			if len(stack) == 0 {
				start = i + 1
			} else {
				stack = stack[:len(stack)-1]

				base := start
				if len(stack) > 0 {
					base = stack[len(stack)-1] + 1
				}
				if i-base+1 > count {
					count = i - base + 1
				}
			}
		}
	}
	return count
}

func main() {
	s := ""
	fmt.Println(longestValidParentheses(s))
}
