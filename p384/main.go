package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Solution struct {
	original []int
}

func Constructor(nums []int) Solution {
	return Solution{original: nums}
}

func (s *Solution) Reset() []int {
	return s.original
}

func (s *Solution) Shuffle() []int {
	n := len(s.original)
	if n == 0 {
		return s.original
	}

	rand.Seed(time.Now().UnixNano())
	shuffled := make([]int, n)
	copy(shuffled, s.original)
	for i := 0; i < n; i++ {
		pos := int(rand.Int31()) % (n - i)
		shuffled[i], shuffled[i+pos] = shuffled[i+pos], shuffled[i]
	}
	return shuffled
}

func main() {
	nums := []int{1, 2, 3, 4, 5, 6, 7}
	s := Constructor(nums)
	fmt.Println(s.Shuffle())
	fmt.Println(s.Shuffle())
	fmt.Println(s.Reset())
}
