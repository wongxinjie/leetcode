package main

import "fmt"

func subsets(nums []int) [][]int {
	result := make([][]int, 0)
	ans := make([]int, 0)
	backtrack(nums, 0, ans, &result)
	return result
}

func backtrack(nums []int, pos int, can []int, rs *[][]int) {
	ans := make([]int, len(can))
	copy(ans, can)
	*rs = append(*rs, ans)

	for i := pos; i < len(nums); i++ {
		can = append(can, nums[i])
		backtrack(nums, i+1, can, rs)
		can = can[:len(can)-1]
	}
}

func main() {
	nums := []int{1, 2, 3}
	fmt.Println(subsets(nums))
}
