package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func buildTree(preorder []int, inorder []int) *TreeNode {
	length := len(preorder)
	if length == 0 {
		return nil
	}

	inorderRootMap := make(map[int]int)
	for i, n := range inorder {
		inorderRootMap[n] = i
	}
	return createTree(preorder, 0, length, inorder, 0, length, inorderRootMap)
}

func createTree(preorder []int, begin, end int, inorder []int, from, to int, inorderRootMap map[int]int) *TreeNode {
	if begin >= end {
		return nil
	}

	if begin+1 == end {
		return &TreeNode{Val: preorder[begin]}
	}

	rootValue := preorder[begin]
	root := &TreeNode{Val: rootValue}

	rootIdx := inorderRootMap[rootValue]

	leftLen := rootIdx - from
	root.Left = createTree(preorder, begin+1, begin+1+leftLen, inorder, from, rootIdx, inorderRootMap)
	root.Right = createTree(preorder, begin+1+leftLen, end, inorder, rootIdx+1, to, inorderRootMap)
	return root
}

func inOrder(root *TreeNode) {
	if root == nil {
		return
	}
	inOrder(root.Left)
	fmt.Print(root.Val, "\t")
	inOrder(root.Right)
}

func preOrder(root *TreeNode) {
	if root == nil {
		return
	}
	fmt.Print(root.Val, "\t")
	preOrder(root.Left)
	preOrder(root.Right)
}

func main() {
	preorder := []int{3, 9, 20, 15, 7}
	inorder := []int{9, 3, 15, 20, 7}
	root := buildTree(preorder, inorder)

	preOrder(root)
	fmt.Println()
	inOrder(root)
}
