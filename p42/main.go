package main

import "fmt"

func trap(height []int) int {

	var (
		area int
		size = len(height)
	)
	for i := 0; i < size; i++ {
		if i == 0 || i == size-1 {
			continue
		}

		leftHeight, rightHeight := height[i], height[i]
		for left := i - 1; left >= 0; left-- {
			if height[left] > leftHeight {
				leftHeight = height[left]
			}
		}
		for right := i + 1; right < size; right++ {
			if height[right] > rightHeight {
				rightHeight = height[right]
			}
		}

		currentMinHeight := leftHeight
		if currentMinHeight > rightHeight {
			currentMinHeight = rightHeight
		}

		remain := currentMinHeight - height[i]
		if remain > 0 {
			area += remain
		}
	}
	return area
}

func main() {
	height := []int{4, 2, 0, 3, 2, 5}
	fmt.Println(trap(height))
}
