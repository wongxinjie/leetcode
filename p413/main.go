package main

import "fmt"

func numberOfArithmeticSlices(nums []int) int {
	n := len(nums)
	if n < 3 {
		return 0
	}

	dp := make([]int, n+1)
	for i := 2; i < n; i++ {
		if nums[i]-nums[i-1] == nums[i-1]-nums[i-2] {
			dp[i] = dp[i-1] + 1
		}
	}

	var count int
	for _, n := range dp {
		count += n
	}
	return count
}

func main() {
	nums := []int{1, 3, 5, 7, 9}
	fmt.Println(numberOfArithmeticSlices(nums))
}
