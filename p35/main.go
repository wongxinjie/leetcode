package main

import "fmt"

func searchInsert(nums []int, target int) int {
	left, right := 0, len(nums)

	for left < right {
		mid := left + (right-left)/2
		if nums[mid] < target {
			left = mid + 1
		} else {
			right = mid
		}
	}

	return left
}

func main() {
	nums := []int{1, 3, 5, 6}
	target := 0
	fmt.Println(searchInsert(nums, target))
}
