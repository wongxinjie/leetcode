package main

import "fmt"

type ListNode struct {
	Val int
	Next *ListNode
}

func deleteDuplicates(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	var (
		prev = head
		current = head.Next
	)
	for current != nil {
		if current.Val != prev.Val {
			prev = current
			current = current.Next
		} else {
			for current != nil && current.Val == prev.Val {
				current = current.Next
			}
			prev.Next = current
		}
	}
	return head
}

func main() {
	head := &ListNode{Val: 1}
	nums := []int{1, 2, 3, 3, 4}

	current := head
	for _, n := range nums {
		node := &ListNode{Val: n}
		current.Next = node
		current = node
	}

	head = deleteDuplicates(head)

	current = head
	for current != nil {
		fmt.Print(current.Val, " ")
		current = current.Next
	}
	fmt.Println()


}
