package main

import (
	"fmt"
)

// TODO: 中等难度
func decodeString(s string) string {

	stack := make([]rune, 0)
	for _, c := range s {
		if c == ']' {

		} else {
			stack = append(stack, c)
		}
	}

	return string(stack)
}

func reverse(s string) string {
	runes := []rune(s)
	length := len(runes)
	for i := 0; i < length/2; i++ {
		runes[i], runes[length-1-i] = runes[length-1-i], runes[i]
	}
	return string(runes)
}

func main() {
	s := "3[a2[c]]"
	fmt.Println(decodeString(s))
}
