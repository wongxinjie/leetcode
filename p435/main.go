package main

import (
	"fmt"
	"sort"
)

func eraseOverlapIntervals(intervals [][]int) int {
	sort.SliceStable(intervals, func(i, j int) bool {
		return intervals[i][1] < intervals[j][1]
	})

	n := len(intervals)
	var (
		total = 0
		prev = intervals[0][1]
	)
	for  i := 1; i < n; i++ {
		// 有交叉的情况
		if intervals[i][0] < prev {
			total += 1
		} else {
			prev = intervals[i][1]
		}
	}
	return total
}

func main() {
	intervals := [][]int{{1, 2},  {2, 3}}
	fmt.Println(eraseOverlapIntervals(intervals))
}
