package main

import (
	"fmt"
	"math"
)

func minSteps(n int) int {
	dp := make([]int, n+1)

	m := int(math.Sqrt(float64(n)))
	for i := 2; i <= n; i++ {
		dp[i] = i
		for j := 2; j <= m; j++ {
			if i%j == 0 {
				dp[i] = dp[j] + dp[i/j]
			}
		}
	}
	return dp[n]
}

func main() {
	fmt.Println(minSteps(3))
}
