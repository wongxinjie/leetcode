package main

import "fmt"

func findAnagrams(s string, p string) []int {

	window := make(map[rune]int)
	need := make(map[rune]int)

	for _, c := range p {
		need[c] += 1
	}

	ss := []rune(s)
	ans := make([]int, 0)
	var left, right, match int
	for right < len(ss) {
		c := ss[right]
		right++

		if _, ok := need[c]; ok {
			window[c]++
			if window[c] == need[c] {
				match++
			}
		}

		if right-left >= len(p) {
			// 已经匹配了
			if right-left == len(p) && match == len(need) {
				ans = append(ans, left)
			}

			remove := ss[left]
			left++

			if _, ok := need[remove]; ok {
				if window[remove] == need[remove] {
					match--
				}
				window[remove]--
			}
		}
	}
	return ans
}

func main() {
	s := "abab"
	p := "ab"

	fmt.Println(findAnagrams(s, p))
}
