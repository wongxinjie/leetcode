package main

import "fmt"

// 使用模拟方法实现栈是否匹配
// https://leetcode-cn.com/problems/validate-stack-sequences/
func validateStackSequences(pushed []int, popped []int) bool {
	length := len(pushed)

	stack := make([]int, 0)
	var match int
	for _, n := range pushed {
		stack = append(stack, n)

		for len(stack) > 0 && match < length && stack[len(stack)-1] == popped[match] {
			stack = stack[:len(stack)-1]
			match++
		}
	}
	return match == length
}

func main() {
	pushed := []int{1, 2, 3, 4, 5}
	popped := []int{4, 3, 5, 1, 2}
	fmt.Println(validateStackSequences(pushed, popped))
}
