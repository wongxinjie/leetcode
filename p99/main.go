package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func recoverTree(root *TreeNode) {
	if root == nil {
		return
	}

	nodes := make([]*TreeNode, 0)
	inOrderTravel(root, &nodes)

	var x, y *TreeNode
	for i := 0; i < len(nodes)-1; i++ {
		if nodes[i].Val > nodes[i+1].Val {
			y = nodes[i+1]
			if x == nil {
				x = nodes[i]
			}
		}
	}

	if x != nil && y != nil {
		x.Val, y.Val = y.Val, x.Val
	}
}

func inOrderTravel(root *TreeNode, nodes *[]*TreeNode) {
	if root == nil {
		return
	}
	inOrderTravel(root.Left, nodes)
	*nodes = append(*nodes, root)
	inOrderTravel(root.Right, nodes)
}

func preOrder(root *TreeNode) {
	if root == nil {
		return
	}
	fmt.Print(root.Val, "\t")
	preOrder(root.Left)
	preOrder(root.Right)
}

func inOrder(root *TreeNode) {
	if root == nil {
		return
	}
	inOrder(root.Left)
	fmt.Print(root.Val, "\t")
	inOrder(root.Right)
}

func main() {
	root := &TreeNode{Val: 3}
	root.Left = &TreeNode{Val: 1}
	root.Right = &TreeNode{Val: 4}
	root.Right.Left = &TreeNode{Val: 2}

	preOrder(root)
	fmt.Println()
	inOrder(root)
	fmt.Println()
	recoverTree(root)

	preOrder(root)
	fmt.Println()
}
