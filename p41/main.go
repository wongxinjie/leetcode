package main

import "fmt"

// 使用字典，打标
// https://leetcode-cn.com/problems/first-missing-positive/
func firstMissingPositive(nums []int) int {
	length := len(nums)
	for i := 0; i < length; i++ {
		if nums[i] <= 0 {
			nums[i] = length + 1
		}
	}

	for i := 0; i < length; i++ {
		n := nums[i]
		if n < 0 {
			n = -n
		}
		if n <= length {
			if nums[n-1] > 0 {
				nums[n-1] = -nums[n-1]
			}
		}
	}

	for i := 0; i < length; i++ {
		if nums[i] > 0 {
			return i + 1
		}
	}
	return length + 1
}

func main() {
	nums := []int{7, 8, 9, 11, 121}
	fmt.Println(firstMissingPositive(nums))

}
