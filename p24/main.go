package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func swapPairs(head *ListNode) *ListNode {
	return swap(head)
}

func swap(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	// 保留下一轮翻转
	nextHead := head.Next.Next
	next := head.Next
	next.Next = head
	head.Next = swapPairs(nextHead)
	return next
}

func main() {
	head := &ListNode{Val: 1}
	head.Next = &ListNode{Val: 2}
	head.Next.Next = &ListNode{Val: 3}
	head.Next.Next.Next = &ListNode{Val: 4}
	head = swapPairs(head)

	current := head
	for current != nil {
		fmt.Print(current.Val, "\t")
		current = current.Next
	}
	fmt.Println()
}
