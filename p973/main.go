package main

import (
	"container/heap"
	"fmt"
)

type Point struct {
	X int
	Y int
}

func (p *Point) Distance() int {
	return p.X*p.X + p.Y*p.Y
}

type Heap []*Point

func (h Heap) Len() int {
	return len(h)
}

func (h Heap) Less(i, j int) bool {
	return h[i].Distance() > h[j].Distance()
}

func (h Heap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *Heap) Push(v interface{}) {
	*h = append(*h, v.(*Point))
}

func (h *Heap) Pop() interface{} {
	origin := *h
	s := len(origin)
	rv := origin[s-1]
	// 避免内存泄漏
	origin[s-1] = nil
	*h = origin[:s-1]
	return rv
}

func kClosest(points [][]int, k int) [][]int {
	h := &Heap{}
	heap.Init(h)

	for _, p := range points {
		point := &Point{
			X: p[0],
			Y: p[1],
		}
		heap.Push(h, point)

		for h.Len() > k {
			heap.Pop(h)
		}
	}

	closePoints := make([][]int, 0, k)
	for h.Len() > 0 {
		p := heap.Pop(h).(*Point)
		closePoints = append(closePoints, []int{p.X, p.Y})
	}

	return closePoints
}

func main() {
	points := [][]int{{3, 3}, {5, -1}, {-2, 4}}
	k := 2
	fmt.Println(kClosest(points, k))
}
