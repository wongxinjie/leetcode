package main

import "fmt"

func maxProfit(prices []int) int {
	n := len(prices)
	if n == 0 {
		return 0
	}

	var (
		costs        = make([]int, n)
		costStates   = make([]int, n)
		profits      = make([]int, n)
		profitStates = make([]int, n)
	)
	costs[0] = -prices[0]
	costStates[0] = costs[0]

	for i := 1; i < n; i++ {
		costs[i] = profitStates[i-1] - prices[i]
		costStates[i] = max(costs[i-1], costStates[i-1])
		profits[i] = max(costs[i-1], costStates[i-1]) + prices[i]
		profitStates[i] = max(profitStates[i-1], profits[i-1])
	}

	return max(profits[n-1], profitStates[n-1])
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func main() {
	prices := []int{1, 2, 3, 0, 2}
	fmt.Println(maxProfit(prices))
}
