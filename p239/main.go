package main

import "fmt"

type Queue struct {
	items []int
}

func (q *Queue) AddLast(v int) {
	q.items = append(q.items, v)
}

func (q *Queue) First() int {
	return q.items[0]
}

func (q *Queue) Last() int {
	i := len(q.items)
	return q.items[i-1]
}

func (q *Queue) RemoveFirst() int {
	v := q.items[0]
	q.items = q.items[1:]
	return v
}

func (q *Queue) RemoveLast() int {
	i := len(q.items) - 1
	v := q.items[i]
	q.items = q.items[:i]
	return v
}

func (q *Queue) IsEmpty() bool {
	return len(q.items) == 0
}

// 单调队列
func maxSlidingWindow(nums []int, k int) []int {
	q := &Queue{}

	windows := make([]int, 0)
	for i := 0; i < len(nums); i++ {
		val := nums[i]

		for !q.IsEmpty() && q.Last() < val {
			q.RemoveLast()
		}
		q.AddLast(val)

		if i < k-1 {
			continue
		}
		windows = append(windows, q.First())

		target := nums[i-k+1]
		if !q.IsEmpty() && q.First() == target {
			q.RemoveFirst()
		}
	}
	return windows
}

func main() {
	nums := []int{1, 3, -1, -3, 5, 3, 6, 7}
	k := 3
	fmt.Println(maxSlidingWindow(nums, k))
}
