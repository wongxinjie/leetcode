package main

import "fmt"

// https://leetcode-cn.com/problems/jump-game/description/
// TODO: 贪心

func canJump(nums []int) bool {
	dp := make([]bool, len(nums))

	dp[0] = true
	for i := 1; i < len(nums); i++ {
		for j := 0; j < i; j++ {
			// 本身能到并且能跳到
			if dp[j] == true && nums[j]+j >= i {
				dp[i] = true
			}
		}
	}
	return dp[len(nums)-1]
}

func main() {
	nums := []int{3, 2, 1, 0, 4}
	fmt.Println(canJump(nums))
}
