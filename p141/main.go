package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func hasCycle(head *ListNode) bool {
	if head == nil || head.Next == nil {
		return false
	}

	fast := head
	slow := head
	for fast.Next != nil && fast.Next.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next

		if fast == slow {
			return true
		}
	}
	return false
}

func main() {
	head := &ListNode{Val: 3}
	nodes := make([]*ListNode, 0)
	nums := []int{2, 0, 4}

	current := head
	for _, n := range nums {
		node := &ListNode{Val: n}
		current.Next = node
		current = node
		nodes = append(nodes, node)
	}
	//nodes[len(nums)-1].Next = nodes[0]

	fmt.Println(hasCycle(head))

}
