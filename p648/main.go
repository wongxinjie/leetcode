package main

import "fmt"

// 684. 冗余连接
// TODO 优化
func findRedundantConnection(edges [][]int) []int {
	n := len(edges)
	uf := NewUF(n + 1)

	for _, e := range edges {
		u, v := e[0], e[1]
		if uf.IsConnect(u, v) {
			return e
		}
		uf.Connect(u, v)
	}
	return []int{-1, -1}
}

type UF struct {
	conn []int
}

func NewUF(n int) *UF {
	conn := make([]int, 0)
	for i := 0; i < n; i++ {
		conn = append(conn, i)
	}
	return &UF{conn: conn}
}

func (u *UF) Find(p int) int{
	for p != u.conn[p] {
		p = u.conn[p]
	}
	return p
}

func (u *UF) Connect(p, q int) {
	u.conn[u.Find(p)] = u.Find(q)
}

func (u *UF) IsConnect(p, q int) bool {
	return u.Find(p) == u.Find(q)
}

func main() {
	edges := [][]int{{1,2}, {2,3}, {3,4}, {1,4}, {1,5}}
	fmt.Println(findRedundantConnection(edges))
}