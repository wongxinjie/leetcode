package main

import "fmt"

func reversePairs(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	var (
		sorted = make([]int, len(nums))
		count  int
	)
	mergeSort(nums, 0, len(nums), &sorted, &count)
	return count
}

func mergeSort(nums []int, begin, end int, sorted *[]int, count *int) {
	if begin >= end || begin+1 >= end {
		return
	}

	mid := begin + (end-begin)/2

	mergeSort(nums, begin, mid, sorted, count)
	mergeSort(nums, mid, end, sorted, count)

	var (
		i  = begin
		j  = mid
		to = begin
	)
	for i < mid || j < end {
		if j >= end || i < mid && nums[i] <= nums[j] {
			(*sorted)[to] = nums[i]
			to++
			i++
			*count += j - mid
		} else {
			(*sorted)[to] = nums[j]
			to++
			j++
		}
	}
	for i := begin; i < end; i++ {
		nums[i] = (*sorted)[i]
	}
}

func main() {
	nums := []int{7, 5, 6, 4}
	fmt.Println(reversePairs(nums))
}
