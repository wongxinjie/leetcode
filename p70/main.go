package main

import "fmt"

func climbStairs(n int) int {
	if n <= 2 {
		return n
	}

	var (
		x = 1
		y = 1
		current int
	)
	for i := 2; i <= n; i++ {
		current = x + y
		x = y
		y = current
	}
	return current
}

func main() {
	fmt.Println(climbStairs(4))
}