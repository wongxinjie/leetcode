package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseKGroup(head *ListNode, k int) *ListNode {
	if head == nil {
		return head
	}

	headGroup := make([]*ListNode, k)
	tailGroup := make([]*ListNode, k)

	n := size(head)
	if n%k != 0 {
		n = n - n%k
	}

	current := head
	for i := 0; i < n; i++ {
		next := current.Next
		current.Next = nil

		if tailGroup[i%k] == nil {
			headGroup[i%k] = current
			tailGroup[i%k] = current
		} else {
			tailGroup[i%k].Next = current
			tailGroup[i%k] = current
		}
		current = next
	}
	remain := current

	dummy := new(ListNode)
	cursor := dummy

	var count int
	for count < n {
		for i := k - 1; i >= 0; i-- {
			slotHead := headGroup[i]
			if slotHead == nil {
				continue
			}
			headGroup[i] = slotHead.Next

			slotHead.Next = nil
			cursor.Next = slotHead
			cursor = slotHead
			count++
		}
	}
	cursor.Next = remain

	return dummy.Next
}

func size(head *ListNode) int {
	var (
		current = head
		length  int
	)
	for current != nil {
		length += 1
		current = current.Next
	}
	return length
}

func buildList(nums []int) *ListNode {
	if len(nums) == 0 {
		return nil
	}

	head := &ListNode{Val: nums[0]}
	current := head
	for i := 1; i < len(nums); i++ {
		node := &ListNode{Val: nums[i]}
		current.Next = node
		current = node
	}
	return head
}

func printList(head *ListNode) {
	current := head
	for current != nil {
		fmt.Print(current.Val, "\t")
		current = current.Next
	}
	fmt.Println()
}

func main() {
	nums := []int{1}
	k := 1
	head := buildList(nums)

	head = reverseKGroup(head, k)
	printList(head)
}
