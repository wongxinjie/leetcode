package main

import "fmt"

func minCut(s string) int {
	if len(s) == 0 || len(s) == 1 {
		return 0
	}

	dp := make([]int, len(s)+1)
	dp[0] = -1

	for i := 1; i <= len(s); i++ {
		dp[i] = i - 1
		for j := 0; j < i; j++ {
			if isPalindrome(s, j, i-1) {
				if dp[j]+1 < dp[i] {
					dp[i] = dp[j] + 1
				}
			}
		}
	}

	return dp[len(s)]
}

func isPalindrome(s string, i, j int) bool {
	for i < j {
		if s[i] != s[j] {
			return false
		}
		i++
		j--
	}
	return true
}

func main() {
	s := "aab"
	fmt.Println(minCut(s))
}
