package main

import (
	"fmt"
	"strconv"
	"strings"
)

func evalRPN(tokens []string) int {

	stack := &Stack{}
	for _, token := range tokens {
		if isOperator(token) {
			x := stack.Pop()
			y := stack.Pop()
			rv := cal(y, x, token)
			stack.Push(rv)
		} else {
			n, _ := strconv.Atoi(token)
			stack.Push(n)
		}
	}

	rv := stack.Pop()
	return rv
}

func isOperator(s string) bool {
	s = strings.TrimSpace(s)
	if s == "+" || s == "-" || s == "*" || s == "/" {
		return true
	}
	return false
}

func cal(x, y int, op string) int {
	switch op {
	case "+":
		return x + y
	case "-":
		return x - y
	case "*":
		return x * y
	case "/":
		return x / y
	}
	return 0
}

type Stack []int

func (s *Stack) Push(n int) {
	*s = append(*s, n)
}

func (s *Stack) Pop() int {
	n := (*s)[len(*s)-1]
	*s = (*s)[:len(*s)-1]
	return n
}

func main() {
	tokens := []string{"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"}
	fmt.Println(evalRPN(tokens))
}
