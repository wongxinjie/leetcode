package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func averageOfLevels(root *TreeNode) []float64 {
	averages := make([]float64, 0)
	queue := make([]*TreeNode, 0)

	if root == nil {
		return averages
	}
	queue = append(queue, root)
	for len(queue) > 0 {
		count := len(queue)

		var total float64
		for i := 0; i < count; i++ {
			node := queue[0]
			queue = queue[1:]
			total += float64(node.Val)
			if node.Left != nil {
				queue = append(queue, node.Left)
			}
			if node.Right != nil {
				queue = append(queue, node.Right)
			}
		}
		averages = append(averages, total / float64(count))
	}

	return averages
}

func main() {
	root := &TreeNode{Val: 3}
	root.Left = &TreeNode{Val: 9}
	root.Right = &TreeNode{Val: 20}
	root.Right.Left = &TreeNode{Val: 15}
	root.Right.Right = &TreeNode{Val: 7}

	fmt.Println(averageOfLevels(root))
}