package main

import "fmt"

func min(x, y int) int {
	if x > y {
		return y
	}
	return x
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func maximalSquare(matrix [][]byte) int {
	if len(matrix) == 0 {
		return 0
	}

	var (
		m       = len(matrix)
		n       = len(matrix[0])
		maxSize int
	)
	dp := make([][]int, m+1)
	for i := 0; i < m+1; i++ {
		dp[i] = make([]int, n+1)
	}

	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			if matrix[i-1][j-1] == '1' {
				dp[i][j] = min(dp[i-1][j-1], min(dp[i][j-1], dp[i-1][j])) + 1
			}
			maxSize = max(maxSize, dp[i][j])
		}
	}
	return maxSize * maxSize
}

func main() {
	matrix := [][]byte{
		{'0'},
	}
	fmt.Println(maximalSquare(matrix))
}
