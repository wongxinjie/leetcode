package main

import (
	"fmt"
	"sort"
)

func subsetsWithDup(nums []int) [][]int {
	rs := make([][]int, 0)
	can := make([]int, 0)

	sort.Ints(nums)
	backtrack(nums, 0, can, &rs)
	return rs
}

func backtrack(nums []int, pos int, can []int, rs *[][]int) {
	ans := make([]int, len(can))
	copy(ans, can)
	*rs = append(*rs, ans)

	for i := pos; i < len(nums); i++ {
		if i != pos && nums[i] == nums[i-1] {
			continue
		}
		can = append(can, nums[i])
		backtrack(nums, i+1, can, rs)
		can = can[:len(can)-1]
	}
}

func main() {
	nums := []int{0}
	fmt.Println(subsetsWithDup(nums))
}
