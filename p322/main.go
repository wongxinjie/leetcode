package main

import "fmt"

func coinChange(coins []int, amount int) int {
	if len(coins) == 0 {
		return -1
	}

	dp := make([]int, amount+1)
	for i := 0; i <= amount; i++ {
		dp[i] = amount + 2
	}
	dp[0] = 0
	for i := 1; i <= amount; i++ {
		for _, coin := range coins {
			if i >= coin {
				if dp[i-coin]+1 < dp[i] {
					dp[i] = dp[i-coin] + 1
				}
			}
		}
	}
	count := dp[amount]
	if count == amount+2 {
		return -1
	}
	return count
}

func main() {
	coins := []int{1, 2, 5}
	fmt.Println(coinChange(coins, 11))
}
