package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}


func isBalanced(root *TreeNode) bool {
	if depth(root) == -1 {
		return false
	}
	return true
}

func depth(root *TreeNode) int {
	if root == nil {
		return 0
	}

	left := depth(root.Left)
	right := depth(root.Right)

	if left == -1 || right == -1 || left - right > 1 || right - left > 1 {
		return -1
	}

	if left > right {
		return left + 1
	}
	return right + 1
}

func main() {
	root := &TreeNode{Val: 1}
	root.Right = &TreeNode{Val: 2}

	left := &TreeNode{Val: 2}
	left.Left = &TreeNode{Val: 3}
	left.Right = &TreeNode{Val: 3}
	root.Left = left
	left.Left.Left = &TreeNode{Val: 4}
	left.Left.Right = &TreeNode{Val: 4}

	fmt.Println(isBalanced(root))
}