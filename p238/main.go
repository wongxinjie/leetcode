package main

import "fmt"

func moveZeroes(nums []int) {
	if len(nums) == 0 || len(nums) == 1 {
		return
	}

	left, right := 0, 0
	// left 之前非0, left->right 区间之间也非0
	for right < len(nums) {
		if nums[right] != 0 {
			nums[left], nums[right] = nums[right], nums[left]
			left++
		}
		right++
	}
}

func main() {
	nums := []int{0, 1, 0, 3, 12}
	moveZeroes(nums)
	fmt.Println(nums)
}
