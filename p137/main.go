package main

import "fmt"

func singleNumber(nums []int) int {
	var ans int

	for i := 0; i < 64; i++ {
		sum := 0
		for j := 0; j < len(nums); j++ {
			sum += (nums[j] >> i) & 1
		}

		ans ^= (sum % 3) << i
	}
	return ans
}

func main() {
	nums := []int{0, 1, 0, 1, 0, 1, 99}
	fmt.Println(singleNumber(nums))
}
