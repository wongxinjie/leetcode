package main

import "fmt"

// 单调栈
// 数组中元素右边第一个比元素自身小的元素的位置
func findRightSmall(nums []int) []int {

	counter := make([]int, len(nums))

	stack := make([]int, 0)
	for i, n := range nums {

		for len(stack) > 0 && nums[stack[len(stack)-1]] > n {
			top := stack[len(stack)-1]
			stack = stack[:len(stack)-1]

			counter[top] = i
		}
		stack = append(stack, i)
	}

	for len(stack) > 0 {
		top := stack[len(stack)-1]
		counter[top] = -1
		stack = stack[:len(stack)-1]
	}

	return counter
}

func main() {
	nums := []int{1, 2, 4, 9, 4, 0, 5}
	fmt.Println(findRightSmall(nums))
}
