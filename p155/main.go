package main

import (
	"fmt"
	"math"
)

type MinStack struct {
	values    []int
	minValues []int
}

func Constructor() MinStack {
	return MinStack{
		values:    make([]int, 0),
		minValues: []int{math.MaxInt32},
	}
}

func (ms *MinStack) Push(val int) {
	ms.values = append(ms.values, val)
	minTop := ms.minValues[len(ms.minValues)-1]
	if val < minTop {
		ms.minValues = append(ms.minValues, val)
	} else {
		ms.minValues = append(ms.minValues, minTop)
	}
}

func (ms *MinStack) Pop() {
	ms.values = ms.values[:len(ms.values)-1]
	ms.minValues = ms.minValues[:len(ms.minValues)-1]
}

func (ms *MinStack) Top() int {
	return ms.values[len(ms.values)-1]
}

func (ms *MinStack) GetMin() int {
	return ms.minValues[len(ms.minValues)-1]
}

func main() {
	s := Constructor()
	s.Push(-2)
	s.Push(0)
	s.Push(-3)
	fmt.Println(s.GetMin())
	s.Pop()
	fmt.Println(s.Top())
	fmt.Println(s.GetMin())
}
