package main

import "fmt"

func permute(nums []int) [][]int {
	ans := make([][]int, 0)
	backtracking(nums, 0, &ans)
	return ans
}

func backtracking(nums []int, level int, ans *[][]int) {
	if level == len(nums)-1 {
		cp := make([]int, len(nums))
		copy(cp, nums)
		*ans = append(*ans, cp)
		return
	}

	for i := level; i < len(nums); i++ {
		nums[i], nums[level] = nums[level], nums[i]
		backtracking(nums, level+1, ans)
		nums[i], nums[level] = nums[level], nums[i]
	}
}

func main() {
	nums := []int{1, 2, 3}
	fmt.Println(permute(nums))
}
