package main

import (
	"fmt"
	"math"
)

func maxProduct(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	product := math.MinInt32
	min, max := 1, 1
	for _, n := range nums {
		if n < 0 {
			max, min = min, max
		}

		if n*max > n {
			max = n * max
		} else {
			max = n
		}
		if n*min < n {
			min = n * min
		} else {
			min = n
		}
		if max > product {
			product = max
		}
	}
	return product
}

func main() {
	nums := []int{2, 3, -2, 4}
	fmt.Println(maxProduct(nums))
}
