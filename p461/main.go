package main

import "fmt"

func hammingDistance(x int, y int) int {
	var count int

	diff := x ^ y
	for diff > 0 {
		count += diff & 1
		diff = diff >> 1
	}
	return count
}

func main() {
	fmt.Println(hammingDistance(2, 2))
}
