package main

import (
	"fmt"
	"sort"
)

func merge(intervals [][]int) [][]int {
	if len(intervals) == 0 {
		return intervals
	}

	sort.SliceStable(intervals, func(i, j int) bool {
		return intervals[i][0] < intervals[j][0]
	})

	mergeResult := make([][]int, 0)
	s, e := intervals[0][0], intervals[0][1]
	for i := 1; i < len(intervals); i++ {
		if intervals[i][0] > e {
			mergeResult = append(mergeResult, []int{s, e})
			s, e = intervals[i][0], intervals[i][1]
		} else {
			if intervals[i][1] > e {
				e = intervals[i][1]
			}
		}
	}

	mergeResult = append(mergeResult, []int{s, e})
	return mergeResult
}

func main() {
	intervals := [][]int{{1, 4}, {2, 3}}
	fmt.Println(merge(intervals))
}
