package main

import "fmt"

// TODO: 滑动窗口
// https://leetcode-cn.com/problems/permutation-in-string/description/
func checkInclusion(s1 string, s2 string) bool {
	window := make(map[rune]int)
	need := make(map[rune]int)

	for _, c := range s1 {
		need[c] += 1
	}

	ss := []rune(s2)

	var left, right, match int
	for right < len(ss) {
		c := ss[right]
		right++

		if _, ok := need[c]; ok {
			window[c]++
			if window[c] == need[c] {
				match++
			}
		}

		if right-left >= len(s1) {
			// 已经匹配了
			if match == len(need) {
				return true
			}

			remove := ss[left]
			left++

			if _, ok := need[remove]; ok {
				if window[remove] == need[remove] {
					match--
				}
				window[remove]--
			}
		}
	}
	return false
}

func main() {
	s1 := "abcdxabcde"
	s2 := "abcdeabcdx"

	fmt.Println(checkInclusion(s1, s2))
}
