package main

import "fmt"

func canPlaceFlowers(flowerbed []int, n int) bool {

	var total int
	size := len(flowerbed)
	for i := 0; i < size; i++ {
		if flowerbed[i] == 0 && (i == size-1 || flowerbed[i+1] == 0) && (i == 0 || flowerbed[i-1] == 0) {
			flowerbed[i] = 1
			total += 1
		}
	}

	return total >= n
}

func main() {
	flowered := []int{1}
	fmt.Println(canPlaceFlowers(flowered, 1))
}
