package main

import (
	"fmt"
	"strings"
)

const (
	min = -2147483648
	max = 2147483647
)

// https://leetcode-cn.com/problems/string-to-integer-atoi/
// 妈的，吃屎的？
func myAtoi(s string) int {
	s = strings.TrimSpace(s)

	var (
		rv    int64
		minus bool
		hasOp bool
	)
	for i, c := range s {
		if c == '-' {
			if i != 0 {
				return 0
			}

			minus = true
			hasOp = true
		} else if c == '+' {
			if i != 0 {
				return 0
			}
			hasOp = true
		} else {
			if c < '0' || c > '9' {
				if hasOp && i == 1 || !hasOp && i == 0 {
					return 0
				} else {
					return myResult(rv, minus)
				}
			} else {
				if rv > max {
					return myResult(rv, minus)
				}
				rv = rv*10 + int64(c-'0')
			}
		}
	}

	return myResult(rv, minus)
}

func myResult(x int64, minus bool) int {
	if minus {
		x = -x
	}

	if x > max {
		return max
	}
	if x < min {
		return min
	}
	return int(x)
}

func main() {
	s := "-5"
	fmt.Println(myAtoi(s))
}
