package main

import "fmt"

func lengthOfLIS(nums []int) int {
	n := len(nums)
	if n <= 1 {
		return n
	}

	dp := make([]int, 0)
	dp = append(dp, nums[0])
	for i := 1; i < n; i++ {
		if dp[len(dp)-1] < nums[i] {
			dp = append(dp, nums[i])
		} else {
			var j int
			for j < len(dp) {
				if dp[j] >= nums[i] {
					break
				}
				j++
			}
			if j < len(dp) {
				dp[j] = nums[i]
			}
		}
	}
	return len(dp)
}

func main() {
	nums := []int{4, 10, 4, 3, 8, 9}
	fmt.Println(lengthOfLIS(nums))
}
