package main

import "fmt"

func maxAreaOfIsland(grid [][]int) int {
	if len(grid) == 0 {
		return 0
	}

	rowCnt, colCnt := len(grid), len(grid[0])
	visited := make([][]bool, rowCnt)
	for i := 0; i < rowCnt; i++ {
		visited[i] = make([]bool, colCnt)
	}

	var area int
	for i := 0; i < rowCnt; i++ {
		for j := 0; j < colCnt; j++ {
			if grid[i][j] == 0 {
				continue
			}
			if visited[i][j] {
				continue
			}
			rv := areaOfIsland(grid, visited, i, j)
			if rv > area {
				area = rv
			}
		}
	}
	return area
}

func areaOfIsland(grid [][]int, visited [][]bool, row, col int) int {

	dirs := [][]int{{0, -1}, {0, 1}, {-1, 0}, {1, 0}}
	queue := make([][]int, 0)
	queue = append(queue, []int{row, col})

	var count int
	for len(queue) > 0 {
		pos := queue[0]
		queue = queue[1:]

		x, y := pos[0], pos[1]
		if visited[x][y] {
			continue
		}
		visited[x][y] = true
		count += 1

		for _, dir := range dirs {
			newRow := x + dir[0]
			newCol := y + dir[1]
			if newRow >= 0 && newRow < len(grid) && newCol >= 0 && newCol < len(grid[0]) {
				if grid[newRow][newCol] == 1 {
					queue = append(queue, []int{newRow, newCol})
				}
			}
		}
	}

	return count
}

func main() {
	grid := [][]int{
		{0, 0, 0, 0, 0, 0, 0, 0},
	}

	area := maxAreaOfIsland(grid)
	fmt.Println(area)
}
