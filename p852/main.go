package main

import "fmt"

func pos(arr []int, i int) int {
	if arr[i-1] < arr[i] && arr[i] < arr[i+1] {
		return -1
	}
	if arr[i-1] < arr[i] && arr[i] > arr[i+1] {
		return 0
	}
	return 1
}

func peakIndexInMountainArray(arr []int) int {
	if len(arr) < 3 {
		return -1
	}

	left, right := 1, len(arr)-1
	for left < right {
		mid := left + (right-left)/2
		p := pos(arr, mid)
		if p < 0 {
			left = mid + 1
		} else {
			right = mid
		}
	}
	return left
}

func main() {
	arr := []int{3, 4, 5, 1}
	fmt.Println(peakIndexInMountainArray(arr))
}
