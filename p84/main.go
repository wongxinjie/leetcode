package main

import "fmt"

// https://leetcode-cn.com/problems/largest-rectangle-in-histogram/
// 动态规划、单调栈
// 单调栈，找到
func largestRectangleArea(heights []int) int {
	if len(heights) == 0 {
		return 0
	}

	stack := make([]int, 0)

	var area int
	for i := 0; i <= len(heights); i++ {
		var current int
		if i == len(heights) {
			current = 0
		} else {
			current = heights[i]
		}

		for len(stack) > 0 && current <= heights[stack[len(stack)-1]] {
			top := stack[len(stack)-1]
			stack = stack[:len(stack)-1]

			h := heights[top]
			w := i
			if len(stack) > 0 {
				peek := stack[len(stack)-1]
				w = i - peek - 1
			}
			if area < h*w {
				area = h * w
			}
		}
		stack = append(stack, i)
	}
	return area
}

func main() {
	heights := []int{2, 6}
	fmt.Println(largestRectangleArea(heights))
}
