package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func generateTrees(n int) []*TreeNode {
	if n == 0 {
		return nil
	}
	return buildTree(1, n)
}

func buildTree(start, end int) []*TreeNode {
	if start > end {
		return []*TreeNode{nil}
	}

	trees := make([]*TreeNode, 0)
	for i := start; i <= end; i++ {
		leftNodes := buildTree(start, i-1)
		rightNodes := buildTree(i+1, end)

		for _, left := range leftNodes {
			for _, right := range rightNodes {
				root := &TreeNode{Val: i}
				root.Left = left
				root.Right = right
				trees = append(trees, root)
			}
		}
	}
	return trees
}

func printTree(root *TreeNode) {
	if root == nil {
		fmt.Print("nil\t")
		return
	}
	fmt.Print(root.Val, "\t")
	printTree(root.Left)
	printTree(root.Right)
}

func main() {
	rvs := generateTrees(3)

	for _, rv := range rvs {
		printTree(rv)
		fmt.Println()
	}
}
