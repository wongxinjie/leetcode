package main

import (
	"fmt"
	"strings"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func tracePath(node *TreeNode, values []int, paths *[][]int) {
	values = append(values, node.Val)
	if node.Left == nil && node.Right == nil {
		path := make([]int, len(values))
		copy(path, values)

		*paths = append(*paths, path)
		return
	}

	if node.Left != nil {
		tracePath(node.Left, values, paths)
	}
	if node.Right != nil {
		tracePath(node.Right, values, paths)
	}
}



func binaryTreePaths(root *TreeNode) []string {
	paths := make([][]int, 0)

	tracePath(root, make([]int, 0), &paths)

	result := make([]string, 0)
	for _, path := range paths {
		s := make([]string, 0)
		for _, c := range path {
			s = append(s, fmt.Sprintf("%d", c))
		}
		result = append(result, strings.Join(s, "->"))
	}

	return result
}

func main() {
	root := &TreeNode{Val: 6}
	root.Left = &TreeNode{Val: 1}
	root.Left.Right = &TreeNode{Val: 3}
	root.Left.Right.Left = &TreeNode{Val: 2}
	root.Left.Right.Right = &TreeNode{Val: 5}
	root.Left.Right.Right.Right = &TreeNode{Val: 4}
	rv := binaryTreePaths(root)
	fmt.Println(rv)

}