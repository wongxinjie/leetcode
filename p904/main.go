package main

import "fmt"

func totalFruit(fruits []int) int {
	left, count := -1, 0
	counter := make(map[int]int)

	for i, n := range fruits {
		counter[n] += 1

		for len(counter) > 2 {
			left++
			origin := fruits[left]
			counter[origin] -= 1
			if counter[origin] == 0 {
				delete(counter, origin)
			}
		}

		if i-left > count {
			count = i - left
		}
	}

	return count
}

func main() {
	fruits := []int{3, 3, 3, 1, 2, 1, 1, 2, 3, 3, 4}
	fmt.Println(totalFruit(fruits))
}
