package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseBetween(head *ListNode, left int, right int) *ListNode {
	if head == nil || left > right {
		return head
	}

	dummyHead := new(ListNode)
	dummyHead.Next = head

	var prev, tail *ListNode
	current := dummyHead
	for i := 0; i < left && current != nil; i++ {
		prev = current
		current = current.Next
	}
	if prev == nil {
		return head
	}

	current = dummyHead
	for i := 0; i < right && current != nil; i++ {
		current = current.Next
	}
	if current != nil {
		tail = current.Next
		current.Next = nil
	}

	rh, rt := reverseList(prev.Next)
	prev.Next = rh
	if rt != nil {
		rt.Next = tail
	}
	return dummyHead.Next
}

func reverseList(head *ListNode) (*ListNode, *ListNode) {
	current := head

	var prev *ListNode
	for current != nil {
		tmp := current.Next
		current.Next = prev
		prev = current
		current = tmp
	}
	return prev, head
}

func print(head *ListNode) {
	current := head
	for current != nil {
		fmt.Print(current.Val, "\t")
		current = current.Next
	}
	fmt.Println()
}

func main() {
	head := &ListNode{Val: 3}
	head.Next = &ListNode{Val: 5}

	//values := []int{2, 3, 4, 5}
	//current := head
	//for _, v := range values {
	//	node := &ListNode{Val: v}
	//	current.Next = node
	//	current = node
	//}

	print(head)
	head = reverseBetween(head, 1, 2)
	print(head)

}
