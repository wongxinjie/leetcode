package main

import "fmt"

type Union struct {
	values []int
	count  int
}

func New(n int) *Union {
	values := make([]int, n)
	for i := 0; i < n; i++ {
		values[i] = i
	}
	return &Union{
		values: values,
		count:  n,
	}
}

func (u *Union) Find(x int) int {
	if x == u.values[x] {
		return x
	}

	return u.Find(u.values[x])
}

func (u *Union) Union(x, y int) {
	if u.Find(x) != u.Find(y) {
		u.count--
	}
	u.values[u.Find(y)] = u.Find(x)
}

func findCircleNum(isConnected [][]int) int {
	if len(isConnected) == 0 || len(isConnected[0]) == 0 {
		return 0
	}

	var (
		row  = len(isConnected)
		col  = len(isConnected[0])
		conn = New(row)
	)
	for i := 0; i < row; i++ {
		for j := 0; j < col; j++ {
			if i == j {
				continue
			}
			if isConnected[i][j] == 1 {
				conn.Union(i, j)
			}
		}
	}

	return conn.count
}

func main() {
	conn := [][]int{
		{1, 1, 0},
		{1, 1, 0},
		{0, 0, 1},
	}

	fmt.Println(findCircleNum(conn))
}
