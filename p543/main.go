package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func diameterOfBinaryTree(root *TreeNode) int {
	var diameter int
	helper(root, &diameter)
	return diameter
}

func helper(root *TreeNode, diameter *int) int {
	if root == nil {
		return 0
	}

	left := helper(root.Left, diameter)
	right := helper(root.Right, diameter)

	if left + right > *diameter {
		*diameter = left + right
	}

	if left > right {
		return left + 1
	} else {
		return right + 1
	}
}

func main() {
	root := &TreeNode{Val: 1}
	root.Left = &TreeNode{Val: 2}
	root.Right = &TreeNode{Val: 3}
	root.Left.Left = &TreeNode{Val: 4}
	root.Left.Right = &TreeNode{Val: 5}

	fmt.Println(diameterOfBinaryTree(root))
}