package main

import "fmt"

func lengthOfLongestSubstring(s string) int {

	left, count := -1, 0
	pos := make(map[rune]int)
	for i, c := range s {
		v, ok := pos[c]
		if ok && v > left {
			left = v
		}
		pos[c] = i

		if i-left > count {
			count = i - left
		}
	}
	return count
}

func main() {
	s := "abcdefg"
	fmt.Println(lengthOfLongestSubstring(s))
}
