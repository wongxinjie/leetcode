package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func reverse(nums []int) []int{
	for i, j := 0, len(nums) - 1; i < j; i, j = i + 1, j - 1 {
		nums[i], nums[j] = nums[j], nums[i]
	}
	return nums
}

func zigzagLevelOrder(root *TreeNode) [][]int {
	lvs := make([][]int, 0)
	if root == nil {
		return lvs
	}

	var (
		queue = make([]*TreeNode, 0)
		right bool
	)
	queue = append(queue, root)
	for len(queue) > 0 {
		lv := make([]int, 0)
		nextLevelQueue := make([]*TreeNode, 0)
		for _, n := range queue {
			lv = append(lv, n.Val)
			if n.Left != nil {
				nextLevelQueue = append(nextLevelQueue, n.Left)
			}
			if n.Right != nil {
				nextLevelQueue = append(nextLevelQueue, n.Right)
			}
		}

		queue = nextLevelQueue
		if right {
			lv = reverse(lv)
		}
		right = !right
		lvs = append(lvs, lv)
	}

	return lvs
}

func main() {
	root := &TreeNode{Val: 3}
	root.Left = &TreeNode{Val: 9}

	right := &TreeNode{Val: 20}
	right.Left = &TreeNode{Val: 15}
	right.Right = &TreeNode{Val: 7}
	root.Right = right

	fmt.Println(zigzagLevelOrder(root))
}