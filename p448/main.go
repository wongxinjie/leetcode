package main

import "fmt"

func findDisappearedNumbers(nums []int) []int {
	size := len(nums)

	for _, n := range nums {
		i := (n - 1) % size
		nums[i] += size
	}

	missing := make([]int, 0)
	for i, n := range nums {
		if n <= size {
			missing = append(missing, i+1)
		}
	}
	return missing
}

func main() {
	nums := []int{1, 1}
	fmt.Println(findDisappearedNumbers(nums))
}
