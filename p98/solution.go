package main

import (
	"fmt"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func checkBST(root, min, max *TreeNode) bool {
	if root == nil {
		return true
	}

	if min != nil && min.Val >= root.Val {
		return false
	}
	if max != nil && max.Val <= root.Val {
		return false
	}

	return checkBST(root.Left, min, root) && checkBST(root.Right, root, max)
}

func isValidBST(root *TreeNode) bool {
	return checkBST(root, nil, nil)
}

func main() {
	root := &TreeNode{Val: 2}
	root.Left = &TreeNode{Val: 1}
	root.Right = &TreeNode{Val: 3}
	//root.Right.Left = &TreeNode{Val: 3}
	//root.Right.Right = &TreeNode{Val: 6}

	fmt.Println(isValidBST(root))
}
