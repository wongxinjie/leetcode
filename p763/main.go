package main

import "fmt"

func partitionLabels(s string) []int {

	lastAppearAt := make(map[rune]int)
	for i, c := range s {
		lastAppearAt[c] = i
	}

	partitions := make([]int, 0)
	var start, end int

	ss := []rune(s)
	for i := 0; i < len(ss); i++ {
		if lastAppearAt[ss[i]] > end {
			end = lastAppearAt[ss[i]]
		}
		if i == end {
			partitions = append(partitions, end-start+1)
			start = end + 1
		}
	}
	return partitions
}

func main() {
	s := "abcabdefg"
	fmt.Println(partitionLabels(s))
}
