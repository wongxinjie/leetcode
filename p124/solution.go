package main

import (
	"fmt"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func maxPathSum(root *TreeNode) int {
	if root == nil {
		return 0
	}

	values := make([]int, 0)
	inOrderTravel(root, &values)

	fmt.Println("values ", values)
	return 0
}

func inOrderTravel(root *TreeNode, values *[]int) {
	if root == nil {
		return
	}
	inOrderTravel(root.Left, values)
	*values = append(*values, root.Val)
	inOrderTravel(root.Right, values)
	return
}

func main() {
	root := &TreeNode{Val: -10}
	root.Left = &TreeNode{Val: 9}
	root.Right = &TreeNode{Val: 20}
	root.Right.Left = &TreeNode{Val: 15}
	root.Right.Right = &TreeNode{Val: 7}

	fmt.Println(maxPathSum(root))

}
