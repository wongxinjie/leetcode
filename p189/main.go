package main

import "fmt"

func rotate(nums []int, k int) {
	s := len(nums)
	// 求余
	k = k % s

	i := 0
	for k > 0 && i < s {
		nums[i], nums[s-k-i] = nums[s-k-i], nums[i]
		i++
		k--
	}
}

func main() {
	nums := []int{1, 2, 3, 4, 5, 6, 7}
	k := 3
	rotate(nums, k)
	fmt.Println(nums)
}
