package main

import (
	"fmt"
	"sort"
)

// TODO:
func permuteUnique(nums []int) [][]int {
	rs := make([][]int, 0)

	sort.Ints(nums)
	backtracking(nums, 0, &rs)
	return rs
}

func backtracking(nums []int, level int, ans *[][]int) {
	if level == len(nums)-1 {
		cp := make([]int, len(nums))
		copy(cp, nums)
		*ans = append(*ans, cp)
		return
	}

	for i := level; i < len(nums); i++ {
		if i > level && nums[i] == nums[level] {
			continue
		}

		nums[i], nums[level] = nums[level], nums[i]
		backtracking(nums, level+1, ans)
		nums[i], nums[level] = nums[level], nums[i]
	}
}

func main() {
	nums := []int{1, 1, 2, 2}
	fmt.Println(permuteUnique(nums))
}
