package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func isPalindrome(head *ListNode) bool {
	if head == nil || head.Next == nil {
		return true
	}

	slow, fast := head, head
	for fast.Next != nil && fast.Next.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
	}

	slow.Next = reverserList(slow.Next)

	fast = head
	slow = slow.Next
	for slow != nil {
		if fast.Val != slow.Val {
			return false
		}
		fast = fast.Next
		slow = slow.Next
	}
	return true
}

func reverserList(head *ListNode) *ListNode {
	var (
		current = head
		prev *ListNode
	)
	for current != nil {
		tmp := current.Next
		current.Next = prev
		prev = current
		current = tmp
	}
	return prev
}

func main() {
	head := &ListNode{Val: 1}
	nums := []int{2, 3, 2, 1}
	current := head
	for _, n := range nums {
		node := &ListNode{Val: n}
		current.Next = node
		current = node
	}

	fmt.Println(isPalindrome(head))
}