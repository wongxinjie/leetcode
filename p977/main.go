package main

import (
	"fmt"
	"sort"
)

func sortedSquares(nums []int) []int {
	ans := make([]int, 0)

	for _, n := range nums {
		ans = append(ans, n*n)
	}

	sort.Ints(ans)
	return ans
}

func main() {
	nums := []int{-4, -1, 0, 3, 10}
	fmt.Println(sortedSquares(nums))
}
