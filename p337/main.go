package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func rob(root *TreeNode) int {
	takeRoot, skipRoot := postOrder(root)
	if takeRoot > skipRoot {
		return takeRoot
	} else {
		return skipRoot
	}
}

func postOrder(root *TreeNode) (int, int) {
	if root == nil {
		return 0, 0
	}

	leftTake, leftSkip := postOrder(root.Left)
	rightTake, rightSkip := postOrder(root.Right)

	take := root.Val + leftSkip + rightSkip
	var skip int
	if leftTake > leftSkip {
		skip += leftTake
	} else {
		skip += leftSkip
	}
	if rightTake > rightSkip {
		skip += rightTake
	} else {
		skip += rightSkip
	}
	return take, skip
}

func main() {
	root := &TreeNode{Val: 3}
	root.Left = &TreeNode{Val: 4}
	root.Right = &TreeNode{Val: 5}
	root.Left.Right = &TreeNode{Val: 1}
	root.Left.Right = &TreeNode{Val: 3}
	root.Right.Right = &TreeNode{Val: 1}

	fmt.Println(rob(root))
}
