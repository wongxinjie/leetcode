package main

import (
	"fmt"
)

func reverseWords(s string) string {
	length := len(s)

	ans := make([]byte, 0)
	for i := 0; i < length; {
		start := i
		for i < length && s[i] != ' ' {
			i++
		}
		for p := start; p < i; p++ {
			ans = append(ans, s[start+i-1-p])
		}

		for i < length && s[i] == ' ' {
			i++
			ans = append(ans, ' ')
		}
	}

	return string(ans)
}

func main() {
	s := "Let's take LeetCode contest"
	fmt.Println(reverseWords(s))
}
