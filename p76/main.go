package main

import (
	"fmt"
)

//  https://kaiwu.lagou.com/course/courseInfo.htm?courseId=685#/detail/pc?id=6699
func minWindow(s string, t string) string {
	if len(s) == 0 || len(t) == 0 {
		return ""
	}

	sStat := make(map[rune]int)
	tStat := make(map[rune]int)
	left := -1

	var (
		equal, unique int
		count         = len(s) + 1
		start         int
	)
	for _, c := range t {
		tStat[c] += 1
		if tStat[c] == 1 {
			unique += 1
		}
	}

	ss := []rune(s)
	for i, c := range ss {
		sStat[c] += 1
		if sStat[c] == tStat[c] {
			equal += 1
		}

		for equal >= unique {
			if count > i-left {
				start = left + 1
				count = i - left
			}

			left++
			t := ss[left]
			if sStat[t] == tStat[t] {
				equal--
			}
			sStat[t]--
		}
	}

	if count <= len(s) {
		return s[start : start+count]
	}
	return ""
}

func main() {
	s := minWindow("ADOBECODEBANC", "ABC")
	fmt.Println(s)
}
