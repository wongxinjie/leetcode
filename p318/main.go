package main

import "fmt"

func maxProduct(words []string) int {
	hashWords := make(map[int]int)

	var rv int
	for _, word := range words {
		var mask int
		size := len(word)
		for _, c := range word {
			mask |= 1 << (c - 'a')
		}
		s := hashWords[mask]
		if size > s {
			hashWords[mask] = size
		}

		for k, v := range hashWords {
			if k&mask == 0 {
				if v*size > rv {
					rv = v * size
				}
			}
		}
	}
	return rv
}

func main() {
	words := []string{"bdcecbcadca", "caafd", "bcadc", "eaedfcd", "fcdecf", "dee", "bfedd", "ffafd", "eceaffa", "caabe", "fbdb", "acafbccaa", "cdc", "ecfdebaafde", "cddbabf", "adc", "cccce", "cbbe", "beedf", "fafbfdcb", "ceecfabedbd", "aadbedeaf", "cffdcfde", "fbbdfdccce", "ccada", "fb", "fa", "ec", "dddafded", "accdda", "acaad", "ba", "dabe", "cdfcaa", "caadfedd", "dcdcab", "fadbabace", "edfdb", "dbaaffdfa", "efdffceeeb", "aefdf", "fbadcfcc", "dcaeddd", "baeb", "beddeed", "fbfdffa", "eecacbbd", "fcde", "fcdb", "eac", "aceffea", "ebabfffdaab", "eedbd", "fdeed", "aeb", "fbb", "ad", "bcafdabfbdc", "cfcdf", "deadfed", "acdadbdcdb", "fcbdbeeb", "cbeb", "acbcafca", "abbcbcbaef", "aadcafddf", "bd", "edcebadec", "cdcbabbdacc", "adabaea", "dcebf", "ffacdaeaeeb", "afedfcadbb", "aecccdfbaff", "dfcfda", "febb", "bfffcaa", "dffdbcbeacf", "cfa", "eedeadfafd", "fcaa", "addbcad", "eeaaa", "af", "fafc", "bedbbbdfae", "adfecadcabe", "efffdaa", "bafbcbcbe", "fcafabcc", "ec", "dbddd", "edfaeabecee", "fcbedad", "abcddfbc", "afdafb", "afe", "cdad", "abdffbc", "dbdbebdbb"}
	fmt.Println(maxProduct(words))
}
