package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

// TODO: Tree
//https://leetcode-cn.com/problems/path-sum-ii/
func pathSum(root *TreeNode, targetSum int) [][]int {
	path := make([]int, 0)
	result := make([][]int, 0)

	search(root, targetSum, &path, &result)
	return result
}

func search(root *TreeNode, target int, path *[]int, result *[][]int) {
	if root == nil {
		return
	}

	*path = append(*path, root.Val)
	if root.Val == target && root.Left == nil && root.Right == nil {
		*result = append(*result, *path)
		return
	}

	search(root.Left, target-root.Val, path, result)
	search(root.Right, target-root.Val, path, result)
}

func main() {
	root := &TreeNode{Val: 5}
	root.Left = &TreeNode{Val: 4}
	root.Right = &TreeNode{Val: 8}
	root.Left.Left = &TreeNode{Val: 11}
	root.Left.Left.Left = &TreeNode{Val: 7}
	root.Left.Left.Right = &TreeNode{Val: 2}
	root.Right = &TreeNode{Val: 8}
	root.Right.Left = &TreeNode{Val: 13}
	root.Right.Right = &TreeNode{Val: 4}
	root.Right.Right.Left = &TreeNode{Val: 5}
	root.Right.Right.Left = &TreeNode{Val: 1}

	fmt.Println(pathSum(root, 22))

}
