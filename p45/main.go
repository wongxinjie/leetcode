package main

import "fmt"

// TODO: 动态规划
// https://leetcode-cn.com/problems/jump-game-ii/description/
func jump(nums []int) int {
	dp := make([]int, len(nums))

	dp[0] = 0
	for i := 1; i < len(nums); i++ {
		dp[i] = i
		for j := 0; j < i; j++ {
			if nums[j]+j >= i {
				if dp[j]+1 < dp[i] {
					dp[i] = dp[j] + 1
				}
			}
		}
	}
	return dp[len(nums)-1]
}

func main() {
	nums := []int{2, 3, 0, 1, 4}
	fmt.Println(jump(nums))
}
