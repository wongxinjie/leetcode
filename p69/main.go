package main

import "fmt"

func mySqrt(x int) int {
	if x == 0 || x == 1 {
		return x
	}

	i := 1
	for i <= x/2 {
		if i*i > x {
			break
		}
		i++
	}
	return i - 1
}

func main() {
	fmt.Println(mySqrt(9))
}
