package main

import "fmt"

func findOrder(numCourses int, prerequisites [][]int) []int {
	graph := make([][]int, numCourses)
	inDegree := make([]int, numCourses)
	order := make([]int, 0)

	for _, p := range prerequisites {
		graph[p[1]] = append(graph[p[1]], p[0])
		inDegree[p[0]]++
	}

	queue := make([]int, 0)
	// 把所有入度为0的顶点全部队列
	for i := 0; i < len(inDegree); i++ {
		if inDegree[i] == 0 {
			queue = append(queue, i)
		}
	}
	for len(queue) > 0 {
		u := queue[0]
		queue = queue[1:]
		order = append(order, u)

		vex := graph[u]
		for _, v := range vex {
			inDegree[v]--
			if inDegree[v] == 0 {
				queue = append(queue, v)
			}
		}
	}

	for i := 0; i < len(inDegree); i++ {
		if inDegree[i] > 0 {
			return make([]int, 0)
		}
	}

	return order
}

func main() {
	pr := [][]int{
		{1, 0},
		{2, 0},
		{3, 1},
		{3, 2},
	}
	fmt.Println(findOrder(4, pr))
}