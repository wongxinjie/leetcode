package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func delNodes(root *TreeNode, to_delete []int) []*TreeNode {
	forest := make([]*TreeNode, 0)
	remove := make(map[int]struct{})
	for _, n := range to_delete {
		remove[n] = struct{}{}
	}

	root = helper(root, remove, &forest)
	if root != nil {
		forest = append(forest, root)
	}

	return forest
}

func helper(root *TreeNode, remove map[int]struct{}, forest *[]*TreeNode)  *TreeNode {
	if root == nil {
		return root
	}

	root.Left = helper(root.Left, remove, forest)
	root.Right = helper(root.Right, remove, forest)

	if _, ok := remove[root.Val]; ok {
		if root.Left != nil {
			*forest = append(*forest, root.Left)
		}
		if root.Right != nil {
			*forest = append(*forest, root.Right)
		}
		root = nil
	}
	return root
}

func main() {
	root := &TreeNode{Val: 1}
	root.Left = &TreeNode{Val: 2}
	root.Right = &TreeNode{Val: 3}
	root.Left.Left = &TreeNode{Val: 4}
	root.Left.Right = &TreeNode{Val: 5}
	root.Right.Left = &TreeNode{Val: 6}
	root.Right.Right = &TreeNode{Val: 7}

	forest := delNodes(root, []int{3, 5})
	fmt.Println(len(forest))
	for _, t := range forest {
		fmt.Println(t.Val)
	}
}
