package main

import "fmt"

func exist(board [][]byte, word string) bool {
	if len(board) == 0 {
		return false
	}

	m := len(board)
	n := len(board[0])
	visited := make([][]bool, 0)
	for i := 0; i < m; i++ {
		visited = append(visited, make([]bool, n))
	}

	var found bool
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			backtracking(board, word, &visited, i, j, 0, &found)
		}
	}
	return found
}

func backtracking(board [][]byte, word string, visited *[][]bool, i, j, pos int, found *bool) {
	if i < 0 || i >= len(board) || j < 0 || j >= len(board[0]) {
		return
	}
	if *found || (*visited)[i][j] || board[i][j] != word[pos] {
		return
	}

	if pos == len(word) - 1 {
		*found = true
		return
	}

	(*visited)[i][j] = true
	backtracking(board, word, visited, i+1, j, pos+1, found)
	backtracking(board, word, visited, i -1, j, pos+1, found)
	backtracking(board, word, visited, i, j+1, pos+1, found)
	backtracking(board, word, visited, i, j-1, pos+1, found)
	(*visited)[i][j] = false
	return
}

func main() {
	board := [][]byte{
		{'A', 'B', 'C', 'E'},
		{'S', 'F', 'C', 'S'},
		{'A', 'D', 'E', 'E'},
	}
	fmt.Println(exist(board, "ABCCED"))
}