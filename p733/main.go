package main

import "fmt"

func floodFill(image [][]int, sr int, sc int, newColor int) [][]int {
	if len(image) == 0 {
		return image
	}

	rowCnt, colCnt := len(image), len(image[0])
	visited := make([][]bool, rowCnt)
	for i := 0; i < rowCnt; i++ {
		visited[i] = make([]bool, colCnt)
	}

	dirs := [][]int{
		{0, -1},
		{0, 1},
		{-1, 0},
		{1, 0},
	}

	targetColor := image[sr][sc]
	queue := make([][]int, 0)
	queue = append(queue, []int{sr, sc})
	for len(queue) > 0 {
		bit := queue[0]
		queue = queue[1:]

		x, y := bit[0], bit[1]
		if visited[x][y] {
			continue
		}
		image[x][y] = newColor
		visited[x][y] = true

		for _, dir := range dirs {
			newSr := x + dir[0]
			newSc := y + dir[1]

			if newSr >= 0 && newSr < rowCnt && newSc >= 0 && newSc < colCnt {
				if image[newSr][newSc] == targetColor {
					queue = append(queue, []int{newSr, newSc})
				}
			}
		}
	}

	return image
}

func main() {
	image := [][]int{
		{0, 0, 0},
		{1, 0, 0},
	}
	floodFill(image, 1, 0, 2)
	fmt.Println(image)
}
