package main

import "fmt"

func numSubarrayProductLessThanK(nums []int, k int) int {
	left := -1
	count := 0
	sum := 1
	for i, n := range nums {
		sum *= n

		for sum >= k && left < i {
			left++
			sum /= nums[left]
		}
		count += i - left
	}
	return count
}

func main() {
	nums := []int{1, 2, 3}
	fmt.Println(numSubarrayProductLessThanK(nums, 0))
}
