package main

import (
	"container/heap"
	"fmt"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

type Heap []*ListNode

func (h Heap) Len() int {
	return len(h)
}

func (h Heap) Less(i, j int) bool {
	return h[i].Val < h[j].Val
}

func (h Heap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *Heap) Push(v interface{}) {
	*h = append(*h, v.(*ListNode))
}

func (h *Heap) Pop() interface{} {
	origin := *h
	s := len(origin)
	rv := origin[s-1]
	// 避免内存泄漏
	origin[s-1] = nil
	*h = origin[:s-1]
	return rv
}

func mergeKLists(lists []*ListNode) *ListNode {
	if len(lists) == 0 {
		return nil
	}
	h := &Heap{}
	heap.Init(h)

	for _, head := range lists {
		current := head
		for current != nil {
			heap.Push(h, current)
			current = current.Next
		}
	}

	var head *ListNode
	if h.Len() == 0 {
		return head
	}

	head = heap.Pop(h).(*ListNode)
	current := head
	for h.Len() > 0 {
		node := heap.Pop(h).(*ListNode)
		current.Next = node
		current = node
	}
	current.Next = nil

	return head
}

func main() {
	nums := [][]int{
		{},
		//{1, 4, 5},
		//{1, 3, 4},
		//{2, 6},
	}

	lists := make([]*ListNode, 0)
	for _, n := range nums {
		lists = append(lists, buildList(n))
	}

	head := mergeKLists(lists)

	current := head
	for current != nil {
		fmt.Print(current.Val, "\t")
		current = current.Next
	}
	fmt.Println()
}

func buildList(nums []int) *ListNode {
	if len(nums) == 0 {
		return nil
	}
	head := &ListNode{Val: nums[0]}
	current := head
	for i := 1; i < len(nums); i++ {
		node := &ListNode{Val: nums[i]}
		current.Next = node
		current = node
	}
	return head
}
