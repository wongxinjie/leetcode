package main

import "fmt"

//给定一个整数数组 nums，按要求返回一个新数组counts。数组 counts 有该性质： counts[i] 的值是 nums[i] 右侧小于nums[i] 的元素的数量。
// [5， 2， 6， 1]
// [2, 1, 1, 0]
func countSmaller(nums []int) []int {
	if len(nums) == 0 {
		return make([]int, 0)
	}

	counter := make([]int, len(nums))
	sorted := make([]int, len(nums))
	idx := make([]int, len(nums))

	for i := 0; i < len(nums); i++ {
		idx[i] = i
	}

	mergeSort(nums, &idx, 0, len(nums), &sorted, &counter)
	return counter
}

func mergeSort(nums []int, idx *[]int, begin, end int, sorted *[]int, counter *[]int) {
	if begin >= end || begin+1 >= end {
		return
	}

	mid := begin + (end-begin)/2

	mergeSort(nums, idx, begin, mid, sorted, counter)
	mergeSort(nums, idx, mid, end, sorted, counter)

	var (
		i  = begin
		j  = mid
		to = begin
	)
	for i < mid || j < end {
		if j >= end || i < mid && nums[(*idx)[i]] <= nums[(*idx)[j]] {
			(*sorted)[to] = (*idx)[i]
			(*counter)[(*idx)[i]] += j - mid
			to++
			i++
		} else {
			(*sorted)[to] = (*idx)[j]
			to++
			j++
		}
	}
	for i := begin; i < end; i++ {
		(*idx)[i] = (*sorted)[i]
	}
}

func main() {
	nums := []int{26, 78, 27, 100, 33, 67, 90, 23, 66, 5, 38, 7, 35, 23, 52, 22, 83, 51, 98, 69, 81, 32, 78, 28, 94, 13, 2, 97, 3, 76, 99, 51, 9, 21, 84, 66, 65, 36, 100, 41}
	fmt.Println(countSmaller(nums))
}
