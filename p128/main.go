package main

import (
	"fmt"
)

func longestConsecutive(nums []int) int {

	counter := make(map[int]struct{})
	for _, n := range nums {
		counter[n] = struct{}{}
	}

	var ans int

	for key := range counter {
		if _, ok := counter[key-1]; ok {
			continue
		}

		n := key
		current := 1
		for {

			_, ok := counter[n+1]
			if ok {
				n += 1
				current += 1
			} else {
				break
			}
		}
		if current > ans {
			ans = current
		}
	}
	return ans
}

func main() {
	nums := []int{1, 2, 0, 1}
	fmt.Println(longestConsecutive(nums))
}
