package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isSubtree(root *TreeNode, subRoot *TreeNode) bool {
	if root == nil &&  subRoot== nil {
		return true
	}
	if root== nil || subRoot == nil {
		return false
	}

	ok := isSameTree(root, subRoot) ||
		isSubtree(root.Left, subRoot) ||
		isSubtree(root.Right, subRoot)
	return ok
}


func isSameTree(a, b *TreeNode) bool {
	if a == nil && b == nil {
		return true
	}
	if a == nil || b == nil {
		return false
	}

	return a.Val == b.Val &&
		isSameTree(a.Left, b.Left) &&
		isSameTree(a.Right, b.Right)
}

func main() {
	a := &TreeNode{Val: 3}
	a.Left = &TreeNode{Val: 4}
	a.Right = &TreeNode{Val: 5}
	a.Left.Left = &TreeNode{Val: 1}
	a.Left.Right = &TreeNode{Val: 2}

	b := &TreeNode{Val: 4}
	b.Left = &TreeNode{Val: 2}
	b.Right = &TreeNode{Val: 1}

	fmt.Println(isSubtree(a, b))

}
