package main

import "fmt"

func reverseBits(num uint32) uint32 {
	var rv uint32
	for i := 0; i < 32; i++ {
		rv <<= 1
		rv += num & 1
		num >>= 1
	}
	return rv
}

func main() {
	fmt.Println(reverseBits(43261596))
}
