package main

import (
	"fmt"
)

func candy(ratings []int) int {
	size := len(ratings)
	if size < 2 {
		return size
	}

	allocate := make([]int, len(ratings))
	for idx := range allocate {
		allocate[idx] = 1
	}

	for i := 0; i < size - 1; i++ {
		if ratings[i+1] > ratings[i] {
			allocate[i+1] = allocate[i] + 1
		}
	}
	for j := size - 1; j > 0; j-- {
		if ratings[j-1] > ratings[j] {
			if allocate[j] + 1 > allocate[j-1] {
				allocate[j-1] = allocate[j] + 1
			}
		}
	}

	total := 0
	for _, r := range allocate {
		total += r
	}

	return total
}

func main() {
	ratings :=[]int{1,3,4,5,2}
	fmt.Println(candy(ratings))
}
