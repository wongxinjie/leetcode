package main

import "fmt"

// https://leetcode-cn.com/problems/surrounded-regions/
// TODO: 并查集和虚节点

func solve(board [][]byte) {
	if len(board) == 0 {
		return
	}

	row, col := len(board), len(board[0])
	for i := 0; i < row; i++ {
		for j := 0; j < col; j++ {
			if i == 0 || j == 0 || i == row-1 || j == col-1 {
				if board[i][j] == 'O' {
					board[i][j] = 'A'
					dfs(&board, i, j)
				}
			}
		}
	}

	for i := 0; i < row; i++ {
		for j := 0; j < col; j++ {
			if board[i][j] != 'A' {
				board[i][j] = 'X'
			} else {
				board[i][j] = 'O'
			}
		}
	}
}

func dfs(board *[][]byte, r, c int) {
	dirs := [][]int{{0, 1}, {0, -1}, {1, 0}, {-1, 0}}

	for _, d := range dirs {
		nr := r + d[0]
		nc := c + d[1]
		if nr < 0 || nr >= len(*board) || nc < 0 || nc >= len((*board)[0]) {
			continue
		}

		if (*board)[nr][nc] == 'O' {
			(*board)[nr][nc] = 'A'
			dfs(board, nr, nc)
		}
	}
}

func f(nums *[][]int) {
	(*nums)[0][1] = 2
}

func main() {
	board := [][]byte{
		{'X', 'X', 'X', 'X'}, {'X', 'O', 'O', 'X'}, {'X', 'X', 'O', 'X'}, {'X', 'O', 'X', 'X'},
	}
	solve(board)
	fmt.Println(board)
}
