package main

import (
	"fmt"
	"sort"
)

func findContentChildren(g, s []int) int {
	sort.Ints(g)
	sort.Ints(s)

	child, cookie := 0, 0
	childSize, cookieSize := len(g), len(s)
	for child < childSize && cookie < cookieSize  {
		if g[child] <= s[cookie] {
			child += 1
		}
		cookie += 1
	}
	return child
}

func main() {
	children := []int{1,2, 3}
	cookies := []int{1, 1}
	fmt.Println(findContentChildren(children, cookies))

}
