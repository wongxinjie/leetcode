package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}


func detectCycle(head *ListNode) *ListNode {
	fast, slow := head, head
	for {
		if fast == nil || fast.Next == nil {
			return nil
		}

		fast = fast.Next.Next
		slow = slow.Next
		if fast == slow {
			break
		}
	}

	fast = head
	for fast != slow {
		fast = fast.Next
		slow = slow.Next
	}
	return fast
}

func buildList(nums []int) *ListNode {
	var root *ListNode
	if len(nums) == 0 {
		return root
	}
	root = &ListNode{Val: nums[0]}
	current := root
	nums = nums[1:]
	for _, n := range nums {
		node := &ListNode{Val: n}
		current.Next = node
		current = node
	}

	return root
}

func main() {
	root := buildList([]int{3, 2, 0, 4})

	tail := root
	for tail.Next != nil {
		tail = tail.Next
	}
	tail.Next = root.Next

	entry := detectCycle(root)
	fmt.Println(entry)


}
