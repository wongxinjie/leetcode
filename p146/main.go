package main

import "fmt"

type innerNode struct {
	key, value int
	prev, next *innerNode
}

type LRUCache struct {
	cap   int
	count int
	head  *innerNode
	tail  *innerNode
	dict  map[int]*innerNode
}

func Constructor(capacity int) LRUCache {
	return LRUCache{
		cap:  capacity,
		head: new(innerNode),
		dict: make(map[int]*innerNode),
	}
}

func (c *LRUCache) Get(key int) int {
	v, ok := c.dict[key]
	if !ok {
		return -1
	}

	c.moveToHead(v)
	return v.value
}

func (c *LRUCache) moveToHead(v *innerNode) {
	if v == c.head.next {
		return
	}

	currPrev := v.prev
	currNext := v.next
	currPrev.next = currNext
	// 后面还有
	if currNext != nil {
		currNext.prev = currPrev
	} else {
		c.tail = currPrev
	}

	original := c.head.next
	original.prev = v
	c.head.next = v
	v.prev = c.head
	v.next = original
}

func (c *LRUCache) removeTail() {
	tmp := c.tail
	prev :=  tmp.prev
	prev.next = nil
	c.tail = prev
	c.count -= 1

	delete(c.dict, tmp.key)
	tmp.prev = nil
}

func (c *LRUCache) Put(key int, value int) {
	v, ok := c.dict[key]
	if ok {
		v.value = value
		c.moveToHead(v)
	} else {
		if c.count == c.cap {
			c.removeTail()
		}

		node := &innerNode{
			key:   key,
			value: value,
		}
		c.dict[key] = node
		c.count += 1

		if c.head.next != nil {
			original := c.head.next
			original.prev = node

			c.head.next = node
			node.prev = c.head
			node.next = original
		} else {
			c.head.next =node
			node.prev = c.head
			c.tail = node
		}
	}
}

func main() {
	c := Constructor(1)
	c.Put(2, 1)
	fmt.Println(c.Get(2))
	c.Put(3, 4)
	fmt.Println(c.Get(2))

}
