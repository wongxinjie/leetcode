package main

import (
	"container/heap"
	"fmt"
)

type Heap []int

func (h Heap) Len() int {
	return len(h)
}

func (h Heap) Less(i, j int) bool {
	return h[i] > h[j]
}

func (h Heap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *Heap) Push(v interface{}) {
	*h = append(*h, v.(int))
}

func (h *Heap) Pop() interface{} {
	origin := *h
	s := len(origin)
	rv := origin[s-1]
	*h = origin[:s-1]
	return rv
}

func furthestBuilding(heights []int, bricks int, ladders int) int {
	if len(heights) == 0 {
		return 0
	}

	h := &Heap{}
	heap.Init(h)
	prev := heights[0]

	var total, pos int

	for i := 1; i < len(heights); i++ {
		current := heights[i]
		if current < prev {
			pos = i
		} else {
			delta := current - prev
			heap.Push(h, delta)
			total += delta

			for total > bricks && ladders > 0 {
				erase := heap.Pop(h).(int)
				total -= erase
				ladders--
			}
			if total <= bricks {
				pos = i
			} else {
				break
			}
		}
		prev = current
	}

	return pos
}

func main() {
	heights := []int{14, 3, 19, 3}
	bricks := 17
	ladders := 2
	fmt.Println(furthestBuilding(heights, bricks, ladders))

}
