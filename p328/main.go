package main

import "fmt"

type ListNode struct {
	Val int
	Next *ListNode
}

func oddEvenList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	var (
		odd = head
		even = head.Next
		evenHead = even
		counter = 1
		current = even.Next
	)
	for current != nil {
		if counter % 2 == 0 {
			even.Next = current
			even = current
		} else {
			odd.Next = current
			odd = current
		}
		counter += 1
		current = current.Next
	}
	odd.Next = evenHead
	even.Next = nil
	return head
}

func main() {
	head := &ListNode{Val: 2}
	nums := []int{1, 3, 5, 6, 4, 7}
	current := head
	for _, n := range nums {
		node := &ListNode{Val: n}
		current.Next = node
		current = node
	}

	head = oddEvenList(head)

	current = head
	for current != nil {
		fmt.Print(current.Val, " ")
		current = current.Next
	}
	fmt.Println()
}