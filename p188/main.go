package main

import (
	"fmt"
	"math"
)

func maxProfit(k int, prices []int) int {
	days := len(prices)
	if days < 2 {
		return 0
	}

	if k >= days {
		return maxProfitUnlimited(prices)
	}

	profits := make([]int, k+1)
	costs := make([]int, k+1)
	for i := 0; i <= k; i++ {
		costs[i] = math.MinInt32
	}

	for i := 0; i < days; i++ {
		for j := 1; j <= k; j++ {
			if profits[j-1]-prices[i] > costs[j] {
				costs[j] = profits[j-1] - prices[i]
			}
			if costs[j]+prices[i] > profits[j] {
				profits[j] = costs[j] + prices[i]
			}
		}
	}
	return profits[k]
}

func maxProfitUnlimited(prices []int) int {
	var profit int
	for i := 1; i < len(prices); i++ {
		if prices[i] > prices[i-1] {
			profit += prices[i] - prices[i-1]
		}
	}
	return profit
}

func main() {
	var (
		k      = 1
		prices = []int{1, 2}
	)
	fmt.Println(maxProfit(k, prices))
}
