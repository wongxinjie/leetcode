package main

import "fmt"

func minPathSum(grid [][]int) int {
	m, n := len(grid), len(grid[0])

	dp := make([]int, n)

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if i == 0 && j == 0 {
				dp[j] = grid[i][j]
			} else if i == 0 {
				dp[j] = dp[j-1] + grid[i][j]
			} else if j == 0 {
				dp[j] = dp[j] + grid[i][j]
			} else {
				if dp[j] < dp[j-1] {
					dp[j] = dp[j] + grid[i][j]
				} else {
					dp[j] = dp[j-1] + grid[i][j]
				}
			}
		}
	}
	return dp[n-1]
}

func main() {
	grid := [][]int{
		{1, 2, 3},
		{4, 5, 6},
	}
	fmt.Println(minPathSum(grid))
}
