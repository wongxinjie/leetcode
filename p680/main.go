package main

import "fmt"

func isPalindrome(runes []rune) (int, int, bool) {
	begin, end := 0, len(runes) -1
	for begin < end {
		if runes[begin] != runes[end] {
			break
		}
		begin++
		end--
	}
	if begin >= end {
		return begin, end, true
	}
	return begin, end, false
}

func validPalindrome(s string) bool {
	runes := []rune(s)
	begin, end, ok := isPalindrome(runes)
	if ok {
		return true
	}

	leftRunes := make([]rune, 0, len(s) -1)
	for idx, r := range runes {
		if idx == end {
			continue
		}
		leftRunes = append(leftRunes, r)
	}
	_, _, ok = isPalindrome(leftRunes)
	if ok {
		return true
	}

	rightRunes := make([]rune, 0, len(s) -1)
	for idx, r := range runes {
		if idx == begin {
			continue
		}
		rightRunes = append(rightRunes, r)
	}
	_, _, ok = isPalindrome(rightRunes)
	if ok {
		return true
	}

	return false
}

func main() {
	ss := []string{"abcdeba"}
	for _, s := range ss {
		fmt.Println(validPalindrome(s))
	}


}
