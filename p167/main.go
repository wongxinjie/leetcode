package main

import "fmt"

func twoSum(numbers []int, target int) []int {
	if len(numbers) == 0 {
		return make([]int, 0)
	}

	begin, end := 0, len(numbers) - 1
	for begin < end {
		if numbers[begin] + numbers[end] == target {
			break
		} else if numbers[begin] + numbers[end] > target {
			end-= 1
		} else {
			begin += 1
		}
	}

	return []int{begin+1, end+1}
}


func main() {
	nums := []int{2, 7, 11, 15}
	fmt.Println(twoSum(nums, 9))

}
