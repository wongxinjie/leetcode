package main

import "fmt"

func rob(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		return nums[0]
	}

	quitFirst := robHouse(nums[1:])
	robFirst := nums[0]
	if len(nums) > 2 {
		robFirst += robHouse(nums[2 : len(nums)-1])
	}

	if quitFirst > robFirst {
		return quitFirst
	} else {
		return robFirst
	}
}

func robHouse(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		return nums[0]
	}
	dp := make([]int, len(nums))
	dp[0], dp[1] = nums[0], nums[1]
	if nums[0] > nums[1] {
		dp[1] = nums[0]
	}
	for i := 2; i < len(nums); i++ {
		if dp[i-2]+nums[i] > dp[i-1] {

			dp[i] = dp[i-2] + nums[i]

		} else {
			dp[i] = dp[i-1]
		}
	}
	return dp[len(nums)-1]
}

func main() {
	nums := []int{2, 3, 2}
	fmt.Println(rob(nums))
}
