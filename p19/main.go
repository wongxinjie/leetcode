package main

import "fmt"

type ListNode struct {
	Val int
	Next *ListNode
}


func removeNthFromEnd(head *ListNode, n int) *ListNode {
	dummyHead := new(ListNode)
	dummyHead.Next = head

	first := dummyHead
	second := dummyHead
	for i := 0; i <= n; i++ {
		first = first.Next
	}

	for first != nil {
		first = first.Next
		second = second.Next
	}
	second.Next = second.Next.Next

	return dummyHead.Next
}

func main() {
	head := &ListNode{Val: 1}
	nums := []int{}
	current := head
	for _, n := range nums {
		node := &ListNode{Val: n}
		current.Next = node
		current = node
	}

	head = removeNthFromEnd(head, 1)
	current = head
	for current != nil {
		fmt.Print(current.Val, " ")
		current = current.Next
	}
	fmt.Println()
}

