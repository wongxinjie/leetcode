package main

import (
	"fmt"
	"math"
)

func maxProfit(prices []int) int {
	var (
		profit int
		cost   = math.MinInt32
	)
	for _, p := range prices {
		// 把价格当成成本
		if -p > cost {
			cost = -p
		}
		// 涨幅就是利润
		if cost+p > profit {
			profit = cost + p
		}
	}
	return profit
}

func main() {
	prices := []int{7, 5, 4, 3, 2, 1}
	fmt.Println(maxProfit(prices))
}
