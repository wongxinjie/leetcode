package main

import "fmt"

func reverseString(s []byte) {
	start, end := 0, len(s)-1

	for start < end {
		s[start], s[end] = s[end], s[start]
		start++
		end--
	}
}

func main() {
	s := []byte{'h', 'e', 'l', 'l'}
	reverseString(s)
	fmt.Println(string(s))
}
