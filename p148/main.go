package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func findMiddle(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	fast := head
	slow := head
	for fast.Next != nil && fast.Next.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
	}
	return slow
}

func mergeSort(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	mid := findMiddle(head)
	back := mid.Next
	mid.Next = nil

	front := mergeSort(head)
	back = mergeSort(back)

	dummy := new(ListNode)
	current := dummy
	for front != nil && back != nil {
		if front.Val < back.Val {
			current.Next = front
			front = front.Next
		} else {
			current.Next = back
			back = back.Next
		}
		current = current.Next
	}
	for front != nil {
		current.Next = front
		front = front.Next
		current = current.Next
	}
	for back != nil {
		current.Next = back
		back = back.Next
		current = current.Next
	}
	return dummy.Next
}

func sortList(head *ListNode) *ListNode {
	return mergeSort(head)
}

func buildList(nums []int) *ListNode {
	if len(nums) == 0 {
		return nil
	}
	head := &ListNode{Val: nums[0]}
	current := head
	for i := 1; i < len(nums); i++ {
		node := &ListNode{Val: nums[i]}
		current.Next = node
		current = node
	}

	return head
}

func printList(head *ListNode) {
	current := head
	for current != nil {
		fmt.Print(current.Val, "\t")
		current = current.Next
	}
	fmt.Println()
}

func main() {
	nums := []int{}
	head := buildList(nums)
	printList(head)

	head = sortList(head)
	printList(head)
}
