package main

import (
	"container/heap"
	"fmt"
)

type Counter struct {
	Word  string
	Count int
}

type WordHeap []*Counter

func (w WordHeap) Len() int {
	return len(w)
}

func (w WordHeap) Less(i, j int) bool {
	if w[i].Count == w[j].Count {
		return w[i].Word > w[j].Word
	}
	return w[i].Count < w[j].Count
}

func (w WordHeap) Swap(i, j int) {
	w[i], w[j] = w[j], w[i]
}

func (w *WordHeap) Push(v interface{}) {
	*w = append(*w, v.(*Counter))
}

func (w *WordHeap) Pop() interface{} {
	origin := *w
	s := len(origin)
	rv := origin[s-1]
	// 避免内存泄漏
	origin[s-1] = nil
	*w = origin[:s-1]
	return rv
}

func topKFrequent(words []string, k int) []string {

	wordCounter := make(map[string]int)
	for _, w := range words {
		wordCounter[w] += 1
	}

	h := &WordHeap{}
	heap.Init(h)

	for w, c := range wordCounter {
		counter := &Counter{Word: w, Count: c}
		heap.Push(h, counter)

		for h.Len() > k {
			heap.Pop(h)
		}
	}

	topWords := make([]string, 0, k)
	for h.Len() > 0 {
		v := heap.Pop(h)
		topWords = append(topWords, v.(*Counter).Word)
	}

	s := len(topWords)
	for i := 0; i < s/2; i++ {
		j := s - i - 1
		topWords[i], topWords[j] = topWords[j], topWords[i]
	}

	return topWords
}

func main() {
	words := []string{"i", "love", "leetcode", "i", "love", "coding"}
	k := 2
	fmt.Println("rv ", topKFrequent(words, k))
}
