package main

import "fmt"

func combine(n int, k int) [][]int {
	ans := make([][]int, 0)
	comb := make([]int, k)

	var count int
	backtracking(&comb, &ans, count, 1, n, k)
	return ans
}

func backtracking(comb *[]int, ans *[][]int, count, p, n, k int) {
	if count == k {
		cp := make([]int, len(*comb))
		copy(cp, *comb)
		*ans = append(*ans, cp)
		return
	}

	for i := p; i <= n; i++ {
		count++
		(*comb)[count] = i
		backtracking(comb, ans, count, i + 1, n, k)
		count--
	}
}

func main() {
	n := combine(4, 2)
	fmt.Println(n)
}