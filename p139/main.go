package main

import "fmt"

// 完全背包问题
func wordBreak(s string, wordDict []string) bool {
	n := len(s)

	dp := make([]bool, n+1)
	dp[0] = true

	for i := 1; i <= n; i++ {
		for _, word := range wordDict {
			size := len(word)
			if i >= size && s[i-size:i] == word {
				dp[i] = dp[i] || dp[i-size]
			}
		}
	}
	return dp[n]
}

func main() {
	s := "applepenapple"
	wordDict := []string{"apple", "pen"}
	fmt.Println(wordBreak(s, wordDict))
}
